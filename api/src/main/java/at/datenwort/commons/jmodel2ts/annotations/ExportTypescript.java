/*
 * Copyright (c) 2022 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package at.datenwort.commons.jmodel2ts.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({TYPE, FIELD, METHOD})
@Retention(RUNTIME)
public @interface ExportTypescript {
    boolean ignore() default false;

    boolean embedded() default false;

    String buildToString() default "";

    TsConstructor constructor() default @TsConstructor(render = false);

    TsImport[] imports() default {};

    TsMethod[] methods() default {};

    boolean enumValueFromToString() default false;

    boolean i18n() default true;

    boolean generateTeplateDirectives() default false;

    String[] addImports() default {};

    String jsonConverter() default "";

    boolean resultForceOptionalProperties() default false;

    DeserializationMode deserializationMode() default DeserializationMode.DEFAULT;

    boolean forceTypeCheck() default false;

    String methodBody() default "";
}
