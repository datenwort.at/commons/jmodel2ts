/*
 * Copyright (c) 2022 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package at.datenwort.commons.jmodel2ts.testData.customType;

import at.datenwort.commons.jmodel2ts.annotations.ExportTypescript;
import at.datenwort.commons.jmodel2ts.api.CustomType;
import at.datenwort.commons.jmodel2ts.app.AbstractModelExporter;
import at.datenwort.commons.jmodel2ts.app.ServiceExporter;
import at.datenwort.commons.jmodel2ts.app.TypescriptGenerator;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PageCustomType implements CustomType {
    private final static Logger log = Logger.getLogger(PageCustomType.class.getName());

    private final Set<String> exportedModels = new HashSet<>();
    private final PropertyNamingStrategies.KebabCaseStrategy kebabCaseStrategy = new PropertyNamingStrategies.KebabCaseStrategy();

    @Override
    public boolean isAssignableFrom(Class<?> type) {
        return Page.class.isAssignableFrom(type);
    }

    @Override
    public Collection<String> getImports(TypescriptGenerator typescriptGenerator, AbstractModelExporter exporter, Type type, boolean addI18n, ExportTypescript exportTypescriptConfig) {
        AbstractModelExporter.TypeInfo typeInfo = exporter.getType(type, 0);
        if (typeInfo.getClazz() != Object.class) {
            String contentClassName = typescriptGenerator.createName(typeInfo.getClazz());
            String modelClassName = "Paged" + contentClassName;
            String path = "model/" + kebabCaseStrategy.translate(modelClassName);

            if (!exportedModels.contains(modelClassName)) {
                exportedModels.add(modelClassName);
                createModel(typescriptGenerator, contentClassName, path);
            }

            return Collections.singleton(String.format(
                    "import { %s } from '../%s'",
                    modelClassName,
                    path));

        } else {
            return Collections.singleton("import { Page } from '@jmodel2ts-custom/pageable'");
        }
    }

    private void createModel(TypescriptGenerator typescriptGenerator, String contentClassName, String path) {
        Path output = typescriptGenerator.getOutputBase().resolve(path + ".ts");

        log.log(Level.INFO, "Export: {0}", output.toAbsolutePath().toString());
        try {
            Files.createDirectories(output.getParent());

            try (PrintWriter writer = new PrintWriter(Files.newBufferedWriter(output, StandardCharsets.UTF_8))) {
                writer.print("""
                        import { Page } from '@jmodel2ts-custom/pageable'
                        import { %s } from '../model/%s'
                                                
                        export class Paged%s extends Page<%s> {
                          @JsonProperty('content', [%s])
                          override content: %s[] = []
                        }""".formatted(
                        contentClassName,
                        kebabCaseStrategy.translate(contentClassName),
                        contentClassName,
                        contentClassName,
                        contentClassName,
                        contentClassName
                ));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Class<?> getType() {
        return Page.class;
    }

    @Override
    public String asParameter(TypescriptGenerator generator, ServiceExporter serviceExporter, Class<?> type, String name, boolean required, TypescriptGenerator typescriptGenerator) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String tsType(TypescriptGenerator typescriptGenerator, AbstractModelExporter exporter, String name, Type type, Map<String, String> genericsReplacement) {
        AbstractModelExporter.TypeInfo typeInfo = exporter.getType(type, 0);
        if (typeInfo.getClazz() != Object.class) {
            return "Paged" + typescriptGenerator.createName(typeInfo.getClazz());
        } else {
            return "Page<any>";
        }
    }

    @Override
    public String jsonType(TypescriptGenerator typescriptGenerator, AbstractModelExporter exporter, ExportTypescript exportTypescriptConfig, Type type, boolean stripArray) {
        AbstractModelExporter.TypeInfo typeInfo = exporter.getType(type, 0);
        if (typeInfo.getClazz() != null) {
            return "Paged" + typescriptGenerator.createName(typeInfo.getClazz());
        } else {
            return "Page<any>";
        }
    }
}
