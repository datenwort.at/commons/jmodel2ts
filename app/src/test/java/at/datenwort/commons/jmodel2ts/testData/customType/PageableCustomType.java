/*
 * Copyright (c) 2022 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package at.datenwort.commons.jmodel2ts.testData.customType;

import at.datenwort.commons.jmodel2ts.annotations.ExportTypescript;
import at.datenwort.commons.jmodel2ts.api.CustomType;
import at.datenwort.commons.jmodel2ts.app.AbstractModelExporter;
import at.datenwort.commons.jmodel2ts.app.ServiceExporter;
import at.datenwort.commons.jmodel2ts.app.TypescriptGenerator;
import org.springframework.data.domain.Pageable;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class PageableCustomType implements CustomType {
    @Override
    public boolean isAssignableFrom(Class<?> type) {
        return Pageable.class.isAssignableFrom(type);
    }

    @Override
    public Class<?> getType() {
        return Pageable.class;
    }

    @Override
    public Collection<String> getImports(TypescriptGenerator typescriptGenerator, AbstractModelExporter exporter, Type type, boolean addI18n, ExportTypescript exportTypescriptConfig) {
        return Collections.singleton("import { Pageable } from '@custom-jmodel2ts/pageable'");
    }

    @Override
    public List<ServiceExporter.ReqParameter> getRequestParameters(TypescriptGenerator typescriptGenerator, ServiceExporter serviceExporter, Type type) {
        return List.of(
                new ServiceExporter.ReqParameter("page", int.class),
                new ServiceExporter.ReqParameter("size", int.class),
                new ServiceExporter.ReqParameter("sort", String[].class)
        );
    }
}
