package at.datenwort.commons.jmodel2ts;

import at.datenwort.commons.jmodel2ts.app.ClassNameConverter;
import at.datenwort.commons.jmodel2ts.app.LocalizationType;
import at.datenwort.commons.jmodel2ts.testData.customType.PageCustomType;
import at.datenwort.commons.jmodel2ts.testData.customType.PageableCustomType;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import static org.assertj.core.api.Assertions.assertThat;

public class TypescriptGeneratorTest {
    private final static Path FILESYSTEM_BASE;

    static {
        try {
            FILESYSTEM_BASE = Files.createTempDirectory("TypescriptGeneratorTest");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @BeforeAll
    public static void generate() {
        var generator = new at.datenwort.commons.jmodel2ts.app.TypescriptGenerator("at.datenwort.commons.jmodel2ts.testData");
        generator.addCustomType(new PageableCustomType());
        generator.addCustomType(new PageCustomType());
        generator.setGenerateRuntimeInfo(true);
        generator.setLocalizationType(LocalizationType.ANGULAR_LOCALIZE);

        //noinspection Convert2Lambda,Anonymous2MethodRef
        generator.setClassNameConverter(new ClassNameConverter() {
            @Override
            public String getNgClassName(Class<?> clazz) {
                return clazz.getSimpleName();
            }
        });

        generator.setOutput(FILESYSTEM_BASE);

        generator.process();
    }

    @AfterAll
    public static void cleanup() throws IOException {
        Files.walkFileTree(FILESYSTEM_BASE, new SimpleFileVisitor<>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                Files.delete(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }

    @Test
    void genEnumTest() throws IOException {
        String reference = """
                import { JsonConverter } from '@jmodel2ts/json2typescript/json-convert-decorators'
                import { EnumConverter } from '@jmodel2ts/json2typescript/common-json-converter'
                import { Component, Input, Pipe, PipeTransform } from '@angular/core'
                
                // noinspection JSUnusedGlobalSymbols
                export enum NgGenEnumTest {
                  ONE = 'ONE',
                  TWO = 'TWO',
                  THREE = 'THREE'
                }
                
                @JsonConverter
                export class NgGenEnumTestConverter extends EnumConverter<NgGenEnumTest> {
                  fromStr(data: string): NgGenEnumTest {
                    return NgGenEnumTest[data as keyof typeof NgGenEnumTest]
                  }
                
                  toStr(data: NgGenEnumTest): string {
                    return NgGenEnumTest[data]
                  }
                }
                
                export class NgGenEnumTestI18n {
                  public static readonly ONE = $localize`:@@enums.NgGenEnumTest.ONE:` || $localize`:@@globals.enums.ONE:ONE`
                  public static readonly TWO = $localize`:@@enums.NgGenEnumTest.TWO:` || $localize`:@@globals.enums.TWO:TWO`
                  public static readonly THREE = $localize`:@@enums.NgGenEnumTest.THREE:` || $localize`:@@globals.enums.THREE:THREE`
                }
                
                @Component({
                  // tslint:disable-next-line:component-selector
                  selector: 'ng-gen-enum-test-i18n',
                  standalone: true,
                  template: `{{ text(key) }}`
                })
                export class NgGenEnumTestI18nComponent {
                  @Input()
                  key: NgGenEnumTest
                
                  text(key: NgGenEnumTest): string {
                    return NgGenEnumTestI18n[key] || $localize`:@@enums.not_defined:No translation`
                  }
                }
                
                @Pipe({
                  name: 'NgGenEnumTestI18n',
                  standalone: true
                })
                export class NgGenEnumTestI18nPipe implements PipeTransform {
                  transform(key: NgGenEnumTest, ...args: any[]): any {
                    if (key == null) {
                      return null
                    }
                
                    return NgGenEnumTestI18n[key] || $localize`:@@enums.not_defined:No translation`
                  }
                }
                """;
        Path path = FILESYSTEM_BASE.resolve("enums/ng-gen-enum-test.enum.ts");
        assertThat(Files.exists(path)).isTrue();

        StringBuilder actualContent = new StringBuilder();
        for (String s : Files.readAllLines(path)) {
            if (!actualContent.isEmpty()) {
                actualContent.append("\n");
            }
            actualContent.append(s);
        }

        assertThat(actualContent).isEqualToIgnoringWhitespace(reference);
    }

    @Test
    void genDtoTest() throws IOException {
        String reference = """
                // noinspection ES6UnusedImports
                import { JsonObject, JsonProperty } from '@jmodel2ts/json2typescript/json-convert-decorators'
                import { FieldDefinition, InstanceDefinition } from '@jmodel2ts/api/runtime'
                import { createProxy } from '@jmodel2ts/api/ts-object-path'
                import { Any } from '@jmodel2ts/json2typescript/any'
                import { LocalDate } from '@js-joda/core'
                import { LocalDateConverter } from '@jmodel2ts/json2typescript/common-json-converter'
                import { LocalDateTime } from '@js-joda/core'
                import { LocalDateTimeConverter } from '@jmodel2ts/json2typescript/common-json-converter'
                import { LocalTime } from '@js-joda/core'
                import { LocalTimeConverter } from '@jmodel2ts/json2typescript/common-json-converter'
                import { MapConverter } from '@jmodel2ts/json2typescript/common-json-converter'
                import { NgGenDtoTestInterface } from './ng-gen-dto-test-interface'
                import { NgGenEnumTest } from '../enums/ng-gen-enum-test.enum'
                import { NgGenEnumTestConverter } from '../enums/ng-gen-enum-test.enum'
                import { ZonedDateTime } from '@js-joda/core'
                import { ZonedDateTimeConverter } from '@jmodel2ts/json2typescript/common-json-converter'
                
                @InstanceDefinition('at.datenwort.commons.jmodel2ts.testData.NgGenDtoTest', 'NgGenDtoTest')
                @JsonObject('NgGenDtoTest')
                export class NgGenDtoTest  implements  NgGenDtoTestInterface {
                  public static readonly _implements_at_datenwort_commons_jmodel2ts_testData_NgGenDtoTestInterface = true
                
                  public readonly lng = NgGenDtoTestI18n
                  public static readonly p = createProxy<NgGenDtoTest>('at.datenwort.commons.jmodel2ts.testData.NgGenDtoTest', 'ngGenDtoTest')
                
                  @FieldDefinition('at.datenwort.commons.jmodel2ts.testData.NgGenDtoTest', 'stringVal', 'java.lang.String', false)
                  @JsonProperty('stringVal', String, true)
                    // @ts-ignore
                  stringVal?: string = undefined
                
                  @FieldDefinition('at.datenwort.commons.jmodel2ts.testData.NgGenDtoTest', 'stringList', 'java.lang.String', true)
                  @JsonProperty('stringList', [String], true)
                    // @ts-ignore
                  stringList?: string[] = undefined
                
                  @FieldDefinition('at.datenwort.commons.jmodel2ts.testData.NgGenDtoTest', 'objectIdArray', '[Ljava.util.UUID;', true)
                  @JsonProperty('objectIdArray', [String], true)
                    // @ts-ignore
                  objectIdArray?: string[] = undefined
                
                  @FieldDefinition('at.datenwort.commons.jmodel2ts.testData.NgGenDtoTest', 'objectIdList', 'java.util.UUID', true)
                  @JsonProperty('objectIdList', [String], true)
                    // @ts-ignore
                  objectIdList?: string[] = undefined
                
                  @FieldDefinition('at.datenwort.commons.jmodel2ts.testData.NgGenDtoTest', 'doubleVal', 'double', false)
                  @JsonProperty('doubleVal', Number)
                    // @ts-ignore
                  doubleVal: number = undefined
                
                  @FieldDefinition('at.datenwort.commons.jmodel2ts.testData.NgGenDtoTest', 'intVal', 'int', false)
                  @JsonProperty('intVal', Number)
                    // @ts-ignore
                  intVal: number = undefined
                
                  @FieldDefinition('at.datenwort.commons.jmodel2ts.testData.NgGenDtoTest', 'integerVal', 'java.lang.Integer', false)
                  @JsonProperty('integerVal', Number, true)
                    // @ts-ignore
                  integerVal?: number = undefined
                
                  @FieldDefinition('at.datenwort.commons.jmodel2ts.testData.NgGenDtoTest', 'genTestEnum', 'at.datenwort.commons.jmodel2ts.testData.NgGenEnumTest', false)
                  @JsonProperty('genTestEnum', NgGenEnumTestConverter, true)
                    // @ts-ignore
                  genTestEnum?: NgGenEnumTest = undefined
                
                  @FieldDefinition('at.datenwort.commons.jmodel2ts.testData.NgGenDtoTest', 'localDate', 'java.time.LocalDate', false)
                  @JsonProperty('localDate', LocalDateConverter, true)
                    // @ts-ignore
                  localDate?: LocalDate = undefined
                
                  @FieldDefinition('at.datenwort.commons.jmodel2ts.testData.NgGenDtoTest', 'localTime', 'java.time.LocalTime', false)
                  @JsonProperty('localTime', LocalTimeConverter, true)
                    // @ts-ignore
                  localTime?: LocalTime = undefined
                
                  @FieldDefinition('at.datenwort.commons.jmodel2ts.testData.NgGenDtoTest', 'localDateTime', 'java.time.LocalDateTime', false)
                  @JsonProperty('localDateTime', LocalDateTimeConverter, true)
                    // @ts-ignore
                  localDateTime?: LocalDateTime = undefined
                
                  @FieldDefinition('at.datenwort.commons.jmodel2ts.testData.NgGenDtoTest', 'zonedDateTime', 'java.time.ZonedDateTime', false)
                  @JsonProperty('zonedDateTime', ZonedDateTimeConverter, true)
                    // @ts-ignore
                  zonedDateTime?: ZonedDateTime = undefined
                
                  @FieldDefinition('at.datenwort.commons.jmodel2ts.testData.NgGenDtoTest', 'comparable', 'java.lang.Comparable', false)
                  @JsonProperty('comparable', Any, true)
                    // @ts-ignore
                  comparable?: any = undefined
                
                  @FieldDefinition('at.datenwort.commons.jmodel2ts.testData.NgGenDtoTest', 'map', 'java.util.Map', false)
                  @JsonProperty('map', MapConverter, true)
                    // @ts-ignore
                  map?: Map<any, any> = undefined
                
                  constructor(data: Partial<NgGenDtoTest> = {}) {
                    Object.assign(this, data)
                  }
                }
                
                export function instanceOfNgGenDtoTest(value: any): value is NgGenDtoTest {
                  return value instanceof NgGenDtoTest
                }
                
                
                export function ifNgGenDtoTest(value: any): NgGenDtoTest | undefined {
                  return value instanceof NgGenDtoTest ? value : undefined
                }
                
                
                export class NgGenDtoTestI18n {
                  public static readonly lStringVal = $localize`:@@optionals.NgGenDtoTest.stringVal:` || $localize`:@@globals.properties.stringVal:stringVal`
                  public static readonly lStringList = $localize`:@@optionals.NgGenDtoTest.stringList:` || $localize`:@@globals.properties.stringList:stringList`
                  public static readonly lObjectIdArray = $localize`:@@optionals.NgGenDtoTest.objectIdArray:` || $localize`:@@globals.properties.objectIdArray:objectIdArray`
                  public static readonly lObjectIdList = $localize`:@@optionals.NgGenDtoTest.objectIdList:` || $localize`:@@globals.properties.objectIdList:objectIdList`
                  public static readonly lDoubleVal = $localize`:@@optionals.NgGenDtoTest.doubleVal:` || $localize`:@@globals.properties.doubleVal:doubleVal`
                  public static readonly lIntVal = $localize`:@@optionals.NgGenDtoTest.intVal:` || $localize`:@@globals.properties.intVal:intVal`
                  public static readonly lIntegerVal = $localize`:@@optionals.NgGenDtoTest.integerVal:` || $localize`:@@globals.properties.integerVal:integerVal`
                  public static readonly lGenTestEnum = $localize`:@@optionals.NgGenDtoTest.genTestEnum:` || $localize`:@@globals.properties.genTestEnum:genTestEnum`
                  public static readonly lLocalDate = $localize`:@@optionals.NgGenDtoTest.localDate:` || $localize`:@@globals.properties.localDate:localDate`
                  public static readonly lLocalTime = $localize`:@@optionals.NgGenDtoTest.localTime:` || $localize`:@@globals.properties.localTime:localTime`
                  public static readonly lLocalDateTime = $localize`:@@optionals.NgGenDtoTest.localDateTime:` || $localize`:@@globals.properties.localDateTime:localDateTime`
                  public static readonly lZonedDateTime = $localize`:@@optionals.NgGenDtoTest.zonedDateTime:` || $localize`:@@globals.properties.zonedDateTime:zonedDateTime`
                  public static readonly lComparable = $localize`:@@optionals.NgGenDtoTest.comparable:` || $localize`:@@globals.properties.comparable:comparable`
                  public static readonly lMap = $localize`:@@optionals.NgGenDtoTest.map:` || $localize`:@@globals.properties.map:map`
                }
                
                (NgGenDtoTest as any).lng = NgGenDtoTestI18n""";
        Path path = FILESYSTEM_BASE.resolve("model/ng-gen-dto-test.ts");
        assertThat(Files.exists(path)).isTrue();

        StringBuilder actualContent = new StringBuilder();
        for (String s : Files.readAllLines(path)) {
            if (!actualContent.isEmpty()) {
                actualContent.append("\n");
            }
            actualContent.append(s);
        }

        assertThat(actualContent).hasToString(reference);
    }

    @Test
    void genDtoTestInterface() throws IOException {
        String reference = """
                // noinspection ES6UnusedImports
                
                export interface NgGenDtoTestInterface {
                  stringVal?: string
                
                  doubleVal: number
                }
                
                export function instanceOfNgGenDtoTestInterface(value: any): value is NgGenDtoTestInterface {
                  return value != null && Object.getPrototypeOf(value).constructor._implements_at_datenwort_commons_jmodel2ts_testData_NgGenDtoTestInterface
                }
                """;
        Path path = FILESYSTEM_BASE.resolve("model/ng-gen-dto-test-interface.ts");
        assertThat(Files.exists(path)).isTrue();

        StringBuilder actualContent = new StringBuilder();
        for (String s : Files.readAllLines(path)) {
            if (!actualContent.isEmpty()) {
                actualContent.append("\n");
            }
            actualContent.append(s);
        }

        assertThat(actualContent).hasToString(reference);
    }

    @Test
    void genServiceTest() throws IOException {
        String reference;
        try (Reader reader = new InputStreamReader(getClass().getResourceAsStream("/service-test-result.txt"), StandardCharsets.UTF_8);
             StringWriter sw = new StringWriter()) {
            reader.transferTo(sw);
            reference = sw.toString();
        }

        Path path = FILESYSTEM_BASE.resolve("services/ng-gen-service-test.service.ts");
        assertThat(Files.exists(path)).isTrue();

        StringBuilder actualContent = new StringBuilder();
        for (String s : Files.readAllLines(path)) {
            if (!actualContent.isEmpty()) {
                actualContent.append("\n");
            }
            actualContent.append(s);
        }

        assertThat(actualContent).isEqualToIgnoringWhitespace(reference);
    }

    @Test
    void genInheritedServiceTest() throws IOException {
        String reference;
        try (Reader reader = new InputStreamReader(getClass().getResourceAsStream("/inherited-service-test-result.txt"), StandardCharsets.UTF_8);
             StringWriter sw = new StringWriter()) {
            reader.transferTo(sw);
            reference = sw.toString();
        }

        Path path = FILESYSTEM_BASE.resolve("services/ng-gen-inherited-service-test.service.ts");
        assertThat(Files.exists(path)).isTrue();

        StringBuilder actualContent = new StringBuilder();
        for (String s : Files.readAllLines(path)) {
            if (!actualContent.isEmpty()) {
                actualContent.append("\n");
            }
            actualContent.append(s);
        }

        assertThat(actualContent).isEqualToIgnoringWhitespace(reference);
    }

    @Test
    void genCustomTypePageTest() throws IOException {
        String reference = """
                import { Page } from '@jmodel2ts-custom/pageable'
                import { NgGenDtoTest } from '../model/ng-gen-dto-test'
                
                export class PagedNgGenDtoTest extends Page<NgGenDtoTest> {
                  @JsonProperty('content', [NgGenDtoTest])
                  override content: NgGenDtoTest[] = []
                }""";
        Path path = FILESYSTEM_BASE.resolve("model/paged-ng-gen-dto-test.ts");
        assertThat(Files.exists(path)).isTrue();

        StringBuilder actualContent = new StringBuilder();
        for (String s : Files.readAllLines(path)) {
            if (!actualContent.isEmpty()) {
                actualContent.append("\n");
            }
            actualContent.append(s);
        }

        assertThat(actualContent).hasToString(reference);
    }
}
