package at.datenwort.commons.jmodel2ts.testData;

import at.datenwort.commons.jmodel2ts.annotations.ExportTypescript;

@ExportTypescript
public interface NgGenDtoTestInterface {
    String getStringVal();

    double getDoubleVal();
}
