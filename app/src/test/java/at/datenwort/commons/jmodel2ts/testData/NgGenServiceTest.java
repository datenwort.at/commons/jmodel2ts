package at.datenwort.commons.jmodel2ts.testData;

import at.datenwort.commons.jmodel2ts.annotations.ExportTypescript;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@RestController
@ExportTypescript
@RequestMapping(
        path = {"/ng_api/serviceTest"},
        produces = {"application/json"}
)
public class NgGenServiceTest {
    @GetMapping({"/all"})
    public List<NgGenDtoTest> getAll() {
        return Collections.emptyList();
    }

    @GetMapping({"/list/{departments}"})
    public SimpleResult listOfNmObjs(@PathVariable("departments") List<NmGenDtoTest> departments) {
        return SimpleResult.OK;
    }

    @GetMapping({"/listOfString/{strings}"})
    public SimpleResult listOfString(@PathVariable("strings") List<String> strings) {
        return SimpleResult.OK;
    }

    @GetMapping({"/arrayOfString/{strings}"})
    public SimpleResult arrayOfString(@PathVariable("strings") String[] strings) {
        return SimpleResult.OK;
    }

    @GetMapping({"/listOfInt/{ints}"})
    public SimpleResult listOfInt(@PathVariable("ints") List<Integer> ints) {
        return SimpleResult.OK;
    }

    @GetMapping({"/enrichedListOfInt/{ints}"})
    public SimpleResult enrichedListOfIntWith(@PathVariable("ints") List<Integer> ints, Enrichment enrichment) {
        return SimpleResult.OK;
    }

    @GetMapping("/pathVariables")
    public SimpleResult pathVariables(
            @RequestParam("commDepartment") NmGenDtoTest commDepartment,
            @RequestParam("commDepartments") List<NmGenDtoTest> commDepartments,
            @RequestParam("word") String word,
            @RequestParam("words") List<String> words,
            @RequestParam("num") int num,
            @RequestParam("nums") List<Integer> nums) {
        return SimpleResult.OK;
    }

    @GetMapping("/optPathVariables")
    public SimpleResult optPathVariables(
            @RequestParam(value = "commDepartment", required = false) NmGenDtoTest commDepartment,
            @RequestParam(value = "commDepartments", required = false) List<NmGenDtoTest> commDepartments,
            @RequestParam(value = "word", required = false) String word,
            @RequestParam(value = "words", required = false) List<String> words,
            @RequestParam(value = "num", required = false) Integer num,
            @RequestParam(value = "nums", required = false) List<Integer> nums) {
        return SimpleResult.OK;
    }

    private SimpleResult optPathVariables(
            @RequestParam(value = "commDepartment", required = false) NmGenDtoTest commDepartment,
            @RequestParam(value = "commDepartments", required = false) List<NmGenDtoTest> commDepartments,
            @RequestParam(value = "word", required = false) String word,
            @RequestParam(value = "words", required = false) List<String> words,
            @RequestParam(value = "num", required = false) Integer num,
            @RequestParam(value = "nums", required = false) List<Integer> nums,
            @RequestParam(value = "plus", required = false) boolean plus) {
        return SimpleResult.OK;
    }

    @GetMapping("/mixPathVariables")
    public SimpleResult mixPathVariables(
            @RequestParam(value = "commDepartment", required = true) NmGenDtoTest commDepartment,
            @RequestParam(value = "commDepartments", required = true) List<NmGenDtoTest> commDepartments,
            @RequestParam(value = "word", required = true) String word,
            @RequestParam(value = "words", required = false) List<String> words,
            @RequestParam(value = "num", required = false) Integer num,
            @RequestParam(value = "nums", required = false) List<Integer> nums) {
        return SimpleResult.OK;
    }

    @GetMapping({"/text"})
    public String getText() {
        return "sure";
    }

    @GetMapping({"/textArray"})
    public String[] getTextArray() {
        return new String[]{"sure1", "sure2"};
    }

    @GetMapping({"/textList"})
    public List<String> getTextList() {
        return Arrays.asList("sure1", "sure2");
    }

    @PostMapping({"/uploadFile"})
    public SimpleResult uploadFile(@RequestBody MultipartFile file) {
        return SimpleResult.OK;
    }

    @GetMapping({"/listPage/{key}"})
    public Page<NgGenDtoTest> listData(@PathVariable String key, Pageable pageable) {
        return Page.empty();
    }

    @GetMapping({"/getReturnsString/{key}"})
    public String getReturnsString(@PathVariable String key) {
        return "yep:" + key;
    }

    @PutMapping({"/putWithoutBodyReturnsString/{key}"})
    public String putWithoutBodyReturnsString(@PathVariable String key,
                                              @RequestParam("limit") int limit) {
        return "yep:" + key;
    }

    @PutMapping({"/putReturnsString/{key}"})
    public String putReturnsString(@PathVariable String key,
                                   @RequestBody() NmGenDtoTest commDepartment) {
        return "yep:" + key;
    }

    @PutMapping({"/putReturnsStringWithLimit/{key}"})
    public String putReturnsStringWithLimit(@PathVariable String key,
                                            @RequestBody() NmGenDtoTest commDepartment,
                                            @RequestParam("limit") int limit) {
        return "yep:" + key;
    }

    @PutMapping({"/getBytes/{key}"})
    public byte[] getBytes(@PathVariable String key,
                           @RequestBody() NmGenDtoTest commDepartment,
                           @RequestParam("limit") int limit) {
        return new byte[10];
    }

    @DeleteMapping({"/removeWithVoid/{key}"})
    public void removeWithVoid(@PathVariable String key) {
    }

    @DeleteMapping({"/removeWithResponseEntityVoid/{key}"})
    public ResponseEntity<Void> removeWithResponseEntityVoid(@PathVariable String key) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .build();
    }

    @DeleteMapping({"/removeWithRequestBody/{key}"})
    public String removeWithRequestBody(@PathVariable String key,
                                        @RequestBody NmGenDtoTest testbody) {
        return "shiny";
    }

    @PostMapping({"/multipart/{key}"})
    public String multipartRequest(@PathVariable String key,
                                   @RequestPart("f1") NmGenDtoTest f1,
                                   @RequestPart("f2") NmGenDtoTest f2,
                                   @RequestPart(value = "f3", required = false) NmGenDtoTest f3) {
        return "shiny";
    }

    @PostMapping({"/multipart/{key}/dto/array"})
    public NgGenDtoTest multipartRequestDtoArray(@PathVariable String key,
                                                 @RequestPart("f1") NmGenDtoTest f1,
                                                 @RequestPart("farray") List<NmGenDtoTest> farray) {
        return null;
    }

    @DeleteMapping({"/multipart/{key}/dto/array"})
    public NgGenDtoTest deleteUsingRequestPart(@PathVariable String key,
                                               @RequestPart("f1") NmGenDtoTest f1) {
        return null;
    }

    @PatchMapping({"/multipart/{key}/dto/array"})
    public NgGenDtoTest updateRequestDtoArray(@PathVariable String key,
                                              @RequestPart("f1") NmGenDtoTest f1,
                                              @RequestPart("data") Set<String> data) {
        return null;
    }
}
