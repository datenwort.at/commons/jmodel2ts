package at.datenwort.commons.jmodel2ts.testData;

import at.datenwort.commons.jmodel2ts.annotations.ExportTypescript;

import java.util.List;
import java.util.UUID;

@ExportTypescript
public class NmGenDtoTest {
    private String stringVal;
    private List<String> stringList;
    private UUID[] objectIdArray;
    private List<UUID> objectIdList;
    private double doubleVal;
    private int intVal;
    private Integer integerVal;
    private NgGenEnumTest genTestEnum;
}
