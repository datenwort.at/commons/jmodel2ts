package at.datenwort.commons.jmodel2ts.testData;

import at.datenwort.commons.jmodel2ts.annotations.ExportAs;
import at.datenwort.commons.jmodel2ts.annotations.ExportTypescript;

@ExportTypescript(ignore = true, embedded = true)
public class Enrichment {
    @ExportAs(tsType = "zoonesProperty<$ENTITY>[]")
    private String[] include;

    @ExportAs(tsType = "zoonesProperty<$ENTITY>[]")
    private String[] exclude;

    public Enrichment(String[] include) {
        this.include = include;
    }

    public String[] getInclude() {
        return include;
    }

    public void setInclude(String[] include) {
        this.include = include;
    }

    public String[] getExclude() {
        return exclude;
    }

    public void setExclude(String[] exclude) {
        this.exclude = exclude;
    }
}
