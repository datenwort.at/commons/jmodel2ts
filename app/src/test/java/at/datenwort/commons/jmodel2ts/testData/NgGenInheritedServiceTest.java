package at.datenwort.commons.jmodel2ts.testData;

import at.datenwort.commons.jmodel2ts.annotations.ExportTypescript;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@ExportTypescript
@RequestMapping(
        path = {"/ng_api/inheritedServiceTest"},
        produces = {"application/json"}
)
public class NgGenInheritedServiceTest extends NgGenServiceTest {
    @GetMapping({"/list/{departments}"})
    public SimpleResult listOfNmObjs(@PathVariable("departments") List<NmGenDtoTest> departments) {
        // do whatever fancy stuff
        return SimpleResult.OK;
    }
}
