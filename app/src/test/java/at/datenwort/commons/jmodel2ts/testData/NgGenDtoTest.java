package at.datenwort.commons.jmodel2ts.testData;

import at.datenwort.commons.jmodel2ts.annotations.ExportTypescript;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@ExportTypescript
public class NgGenDtoTest implements NgGenDtoTestInterface {
    private String stringVal;
    private List<String> stringList;
    private UUID[] objectIdArray;
    private List<UUID> objectIdList;
    private double doubleVal;
    private int intVal;
    private Integer integerVal;
    private NgGenEnumTest genTestEnum;

    private LocalDate localDate;
    private LocalTime localTime;
    private LocalDateTime localDateTime;
    private ZonedDateTime zonedDateTime;

    private Comparable comparable;

    private Map map;

    @Override
    public String getStringVal() {
        return stringVal;
    }

    @Override
    public double getDoubleVal() {
        return doubleVal;
    }
}
