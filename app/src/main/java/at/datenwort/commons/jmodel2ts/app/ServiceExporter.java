/*
 * Copyright (c) 2022 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package at.datenwort.commons.jmodel2ts.app;

import at.datenwort.commons.jmodel2ts.annotations.DeserializationMode;
import at.datenwort.commons.jmodel2ts.annotations.ExportAs;
import at.datenwort.commons.jmodel2ts.annotations.ExportTypescript;
import at.datenwort.commons.jmodel2ts.annotations.TsImport;
import at.datenwort.commons.jmodel2ts.api.CustomType;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.core.annotation.MergedAnnotation;
import org.springframework.core.annotation.MergedAnnotations;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ValueConstants;
import org.springframework.web.multipart.MultipartFile;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

public class ServiceExporter extends AbstractModelExporter {
    public static class ReqParameter {
        private final String name;
        private final Class<?> type;
        private final Type genericType;
        private final Annotation[] annotations;

        public ReqParameter(String name, Class<?> type, Type genericType, Annotation... annotations) {
            this.name = name;
            this.type = type;
            this.genericType = genericType;
            this.annotations = annotations;
        }

        public ReqParameter(String name, Class<?> type, Type genericType) {
            this(name, type, genericType, defaultRequestParam(name));
        }

        public ReqParameter(String name, Class<?> type) {
            this(name, type, null);
        }

        public String getName() {
            return name;
        }

        public Class<?> getType() {
            return type;
        }

        public Type getGenericType() {
            return genericType;
        }

        public Annotation[] getAnnotations() {
            return annotations;
        }
    }

    private final Set<Class<?>> dtos;

    public ServiceExporter(TypescriptGenerator typescriptGenerator, Class<?> clazz, String name, String baseDir, Set<Class<?>> dtos) {
        super(typescriptGenerator, clazz, name, baseDir);
        this.dtos = dtos;
    }

    public Set<Class<?>> getDtos() {
        return dtos;
    }

    public static RequestParam defaultRequestParam(final String parameterName) {
        return new RequestParam() {
            public Class<? extends Annotation> annotationType() {
                return RequestParam.class;
            }

            public @NotNull String value() {
                return parameterName;
            }

            public @NotNull String name() {
                return parameterName;
            }

            public boolean required() {
                return false;
            }

            public @NotNull String defaultValue() {
                return ValueConstants.DEFAULT_NONE;
            }
        };
    }

    public void export(PrintWriter writer) {
        Set<String> imports = new TreeSet<>();

        List<Method> declaredMethods = getDeclaredMethods(clazz);

        Class<?> exportClass = clazz.getSuperclass();
        while (exportClass != null && exportClass != Object.class) {
            //also export superclass methods
            declaredMethods.addAll(getDeclaredMethods(exportClass));
            exportClass = exportClass.getSuperclass();
        }

        // Arrays.sort(declaredMethods, Comparator.comparing(Method::getName));

        boolean withProgressImports = false;

        for (Method method : declaredMethods) {
            MergedAnnotation<RequestMapping> methodMapping = MergedAnnotations.from(method).get(RequestMapping.class);
            if (!methodMapping.isPresent()) {
                continue;
            }

            RequestMethod requestMethod = Toolbox.getFirstNonNull(methodMapping.getEnumArray("method", RequestMethod.class));

            RequestBodyHelper bodyHelper = new RequestBodyHelper(requestMethod, method);
            if (bodyHelper.withProgress()) {
                withProgressImports = true;
            }

            if (Modifier.isStatic(method.getModifiers())) {
                continue;
            }
            if (!Modifier.isPublic(method.getModifiers())) {
                continue;
            }

            imports.addAll(tsImports(
                    method.getAnnotation(ExportTypescript.class),
                    method.getReturnType(), method.getGenericReturnType(), false, false));
            for (Parameter parameter : method.getParameters()) {
                ExportTypescript exportTypescript = parameter.getType().getAnnotation(ExportTypescript.class);
                if (exportTypescript != null) {
                    for (TsImport tsImport : exportTypescript.imports()) {
                        if (!ObjectUtils.isEmpty(tsImport.value())) {
                            imports.add(tsImport.value());
                        }
                    }
                }
            }
            for (ReqParameter reqParameter : getParameters(method)) {
                ExportTypescript reqParameterExportConfig = reqParameter.type.getAnnotation(ExportTypescript.class);
                if (reqParameterExportConfig != null && reqParameterExportConfig.ignore()) {
                    continue;
                }

                imports.addAll(tsImports(
                        findAnnotation(reqParameter.annotations, ExportTypescript.class),
                        reqParameter.type,
                        reqParameter.genericType, false, false));
            }
        }
        imports.forEach(writer::println);

        MergedAnnotation<RequestMapping> mapping = MergedAnnotations.from(clazz).get(RequestMapping.class);
        String serviceBasePath = Toolbox.getFirstNonNull(mapping.getStringArray("path"));

        StringWriter writerInterfaceBuffer = new StringWriter();
        PrintWriter writerInterface = null;
        if (typescriptGenerator.isMockableServices()) {
            writerInterface = new PrintWriter(writerInterfaceBuffer);
        }

        writer.println("// noinspection ES6UnusedImports");
        writer.println("import { HttpClient, HttpParams, HttpContext" + (withProgressImports ? ", HttpResponse, HttpEventType, HttpProgressEvent" : "") + " } from '@angular/common/http'");
        writer.println("// noinspection ES6UnusedImports");
        writer.println("import { JsonToTypedUtils } from '@jmodel2ts/json2typescript/json-to-typed-utils'");
        writer.println("// noinspection ES6UnusedImports");
        writer.println("import { HttpUtils, HttpParamsBuilder, HttpFormDataBuilder, HTTP_CONTEXT_TOKEN, HttpRequestContext } from '@jmodel2ts/json2typescript/http-utils'");
        if (!typescriptGenerator.isMockableServices()) {
            writer.println("import { Injectable } from '@angular/core'");
        }

        writer.println("import { Observable, of" + (withProgressImports ? ", Subject" : " ") + ", throwError} from 'rxjs'");
        if (withProgressImports) {
            writer.println("import { filter, map, tap } from 'rxjs/operators'");
        }
        writer.println("import { Any } from '@jmodel2ts/json2typescript/any'");
        writer.println();
        writer.println("// noinspection JSUnusedLocalSymbols");
        if (typescriptGenerator.isMockableServices()) {
            writer.format("export class %sServiceImpl implements %sService {\n", typescriptGenerator.createName(clazz), typescriptGenerator.createName(clazz));
        } else {
            writer.println("@Injectable({ providedIn: 'root' })");
            writer.format("export class %sService {\n", typescriptGenerator.createName(clazz));
        }
        if (writerInterface != null) {
            writerInterface.format("export abstract class %sService {\n", typescriptGenerator.createName(clazz));
        }
        writer.println();
        writer.println("  constructor(");
        writer.println("    private http: HttpClient,");
        writer.println("    private httpUtils: HttpUtils,");
        writer.println("    private jsonToTypedUtils: JsonToTypedUtils) {");
        writer.println("  }");

        Set<String> seenMethods = new HashSet<>();

        for (Method method : declaredMethods) {
            if (Modifier.isStatic(method.getModifiers())) {
                continue;
            }
            if (!Modifier.isPublic(method.getModifiers())) {
                continue;
            }

            MergedAnnotation<RequestMapping> methodMapping = MergedAnnotations.from(method).get(RequestMapping.class);
            if (!methodMapping.isPresent()) {
                continue;
            }

            if (seenMethods.contains(method.getName())) {
                // each method has to have a unique name to be exported
                continue;
            }

            seenMethods.add(method.getName());

            List<ReqParameter> reqParameters = getParameters(method);

            String methodBasePath = Toolbox.getFirstNonNull(methodMapping.getStringArray("path"));
            RequestMethod requestMethod = Toolbox.getFirstNonNull(methodMapping.getEnumArray("method", RequestMethod.class));

            RequestBodyHelper requestBodyHelper = new RequestBodyHelper(requestMethod, method);

            Map<String, String> genericsReplacement = new HashMap<>();
            Type returnType = method.getGenericReturnType();

            CustomType customType = typescriptGenerator.findCustomTypeFor(method.getReturnType());
            if (customType != null) {
                returnType = findClass(method.getGenericReturnType(), customType.getType());
            } else if (List.class.isAssignableFrom(method.getReturnType())) {
                returnType = findClass(method.getGenericReturnType(), List.class);
            }
            TypeInfo returnTypeInfo;
            if (returnType instanceof ParameterizedType) {
                returnTypeInfo = getType(returnType, 0);
            } else {
                returnTypeInfo = new TypeInfo((Class<?>) returnType);
            }

            String entityReturnType = tsType("method return", returnTypeInfo, returnType, true, null);
            genericsReplacement.put("ENTITY", entityReturnType);
            genericsReplacement.put("$ENTITY", entityReturnType);


            String servicePath = Toolbox.joinPath(serviceBasePath, methodBasePath);
            String httpContext = buildHttpContext(typescriptGenerator.createName(clazz), method.getName(), reqParameters);
            String httpParams = buildHttpQueryParams(reqParameters, genericsReplacement);
            String urlParams = buildHttpUrlParams(reqParameters, genericsReplacement);
            String formDataParams = buildFormDataParams(reqParameters, genericsReplacement);

            writer.println();

            if (writerInterface != null) {
                writerInterface.format("""                    
                                  /**
                                   * @see {@link %s#%s}
                                   */
                                """,
                        clazz.getName(),
                        method.getName());
            } else {
                writer.format("""                    
                                  /**
                                   * @see {@link %s#%s}
                                   */
                                """,
                        clazz.getName(),
                        method.getName());
            }

            boolean serviceWritten = false;
            String tsReturnType = tsType(method.getName(), new TypeInfo(method.getReturnType()), method.getGenericReturnType(), true, genericsReplacement);
            if ("url".equals(tsReturnType)) {
                serviceWritten = true;
                writer.format("  %sUrl(",
                        method.getName());
                writeArguments(
                        method,
                        writer,
                        reqParameters,
                        genericsReplacement,
                        requestBodyHelper.withProgress());
                writer.println("): string {");

                if (writerInterface != null) {
                    writerInterface.format("  %sUrl(",
                            method.getName());
                    writeArguments(
                            method,
                            writerInterface,
                            reqParameters,
                            genericsReplacement,
                            requestBodyHelper.withProgress());
                    writerInterface.println("): string { throw new Error('not implemented') }");
                    writerInterface.println();
                }

                writer.println(httpContext);

                if (!httpParams.isEmpty()) {
                    writer.print(httpParams);
                }
                if (!formDataParams.isEmpty()) {
                    writer.print(formDataParams);
                }

                if (requestBodyHelper.hasRequestBodyWrapper()) {
                    writer.write(requestBodyHelper.getRequestBodyWrapper());
                    writer.println();
                }

                writer.println("    // tslint:disable-next-line:max-line-length");
                writer.format("    return %s'/%s'%s%s\n",
                        !urlParams.isEmpty() ? "this.httpUtils.pathEncode(" : "",
                        servicePath,
                        !urlParams.isEmpty() ? ", " + urlParams + ")" : "",
                        !httpParams.isEmpty() ? " + this.httpUtils.buildQueryFragment(params)" : "");


            }
            if (!serviceWritten && ("string".equals(tsReturnType) || "Blob".equals(tsReturnType))) {
                serviceWritten = true;

                writer.format("  %s(",
                        method.getName());
                writeArguments(
                        method,
                        writer,
                        reqParameters,
                        genericsReplacement,
                        requestBodyHelper.withProgress());
                writer.println(
                        String.format("): Observable<%s> {",
                                "string".equals(tsReturnType) ? "string" : "Blob"));

                if (writerInterface != null) {
                    writerInterface.format("  %s(",
                            method.getName());
                    writeArguments(
                            method,
                            writerInterface,
                            reqParameters,
                            genericsReplacement,
                            requestBodyHelper.withProgress());
                    writerInterface.println(
                            String.format("): Observable<%s> { return throwError(() => new Error('not implemented')) }",
                                    "string".equals(tsReturnType) ? "string" : "Blob"));
                    writerInterface.println();
                }

                writer.println(httpContext);

                if (!httpParams.isEmpty()) {
                    writer.print(httpParams);
                }
                if (!formDataParams.isEmpty()) {
                    writer.print(formDataParams);
                }

                if (requestBodyHelper.hasRequestBodyWrapper()) {
                    writer.write(requestBodyHelper.getRequestBodyWrapper());
                    writer.println();
                }

                writer.format("    return this.http.%s(%s'/%s'%s%s%s)%s\n",
                        toHttpClientMethod(requestMethod),
                        !urlParams.isEmpty() ? "this.httpUtils.pathEncode(" : "",
                        servicePath,
                        !urlParams.isEmpty() ? ", " + urlParams + ")" : "",
                        requestBodyHelper.getRequestBody(),
                        toParams(httpContext, httpParams, requestBodyHelper, "string".equals(tsReturnType) ? "text" : "blob"),
                        addOptionalResultPipe(requestBodyHelper.withProgress()));
            }
            if (!serviceWritten && customType != null) {
                if (customType.writeService(typescriptGenerator, this, method.getReturnType(),
                        requestBodyHelper,
                        writer, writerInterface,
                        method, reqParameters, genericsReplacement, tsReturnType, httpContext, httpParams, urlParams,
                        requestMethod, servicePath)) {
                    serviceWritten = true;
                }
            }
            if (!serviceWritten) {
                writer.format("  %s(",
                        method.getName());
                writeArguments(
                        method,
                        writer,
                        reqParameters,
                        genericsReplacement,
                        requestBodyHelper.withProgress());
                writer.format("): Observable<%s> {\n",
                        tsReturnType);

                if (writerInterface != null) {
                    writerInterface.format("  %s(",
                            method.getName());
                    writeArguments(
                            method,
                            writerInterface,
                            reqParameters,
                            genericsReplacement,
                            requestBodyHelper.withProgress());
                    writerInterface.format("): Observable<%s> { return %s }\n\n",
                            tsReturnType,
                            isArray(method.getReturnType()) ? "of([])" : "throwError(() => new Error('not implemented'))");
                }


                writer.println(httpContext);

                if (!httpParams.isEmpty()) {
                    writer.print(httpParams);
                }
                if (!formDataParams.isEmpty()) {
                    writer.print(formDataParams);
                }

                if (requestBodyHelper.hasRequestBodyWrapper()) {
                    writer.write(requestBodyHelper.getRequestBodyWrapper());
                    writer.println();
                }

                writer.println("    // tslint:disable-next-line:max-line-length");
                if (isPrimitive(tsReturnType)) {
                    writer.format("    return this.http.%s<%s>(%s'/%s'%s%s%s)%s\n",
                            toHttpClientMethod(requestMethod),
                            tsReturnType,
                            !urlParams.isEmpty() ? "this.httpUtils.pathEncode(" : "",
                            servicePath,
                            !urlParams.isEmpty() ? ", " + urlParams + ")" : "",
                            requestBodyHelper.getRequestBody(),
                            toParams(httpContext, httpParams, requestBodyHelper, null),
                            addOptionalResultPipe(requestBodyHelper.withProgress()));
                } else {
                    ExportTypescript exportTypescript = method.getAnnotation(ExportTypescript.class);
                    DeserializationMode mode;
                    if (exportTypescript != null) {
                        mode = exportTypescript.deserializationMode();
                    } else if ("void".equals(jsonType(method.getAnnotation(ExportTypescript.class),
                            new TypeInfo(method.getReturnType()), method.getGenericReturnType(),
                            true, false))) {
                        mode = DeserializationMode.VOID;
                    } else {
                        mode = DeserializationMode.DEFAULT;
                    }
                    boolean resultForceOptionalProperties = exportTypescript != null && exportTypescript.resultForceOptionalProperties();

                    switch (mode) {
                        case DEFAULT ->
                                writer.format("    return this.jsonToTypedUtils.json2Typed%s(this.http.%s(%s'/%s'%s%s%s)%s, %s)\n",
                                        isArray(method.getReturnType()) ? "Array" : "",
                                        toHttpClientMethod(requestMethod),
                                        !urlParams.isEmpty() ? "this.httpUtils.pathEncode(" : "",
                                        servicePath,
                                        !urlParams.isEmpty() ? ", " + urlParams + ")" : "",
                                        requestBodyHelper.getRequestBody(),
                                        toParams(httpContext, httpParams, requestBodyHelper, null),
                                        addOptionalResultPipe(requestBodyHelper.withProgress()),
                                        jsonType(method.getAnnotation(ExportTypescript.class),
                                                new TypeInfo(method.getReturnType()), method.getGenericReturnType(),
                                                true, false));
                        case VOID -> writer.format("    return this.http.%s<%s>(%s'/%s'%s%s%s)%s\n",
                                toHttpClientMethod(requestMethod),
                                jsonType(method.getAnnotation(ExportTypescript.class),
                                        new TypeInfo(method.getReturnType()), method.getGenericReturnType(),
                                        true, false),
                                !urlParams.isEmpty() ? "this.httpUtils.pathEncode(" : "",
                                servicePath,
                                !urlParams.isEmpty() ? ", " + urlParams + ")" : "",
                                requestBodyHelper.getRequestBody(),
                                toParams(httpContext, httpParams, requestBodyHelper, null),
                                addOptionalResultPipe(requestBodyHelper.withProgress()));
                        case USE_META ->
                                writer.format("    return this.jsonToTypedUtils.jsonWithMeta2Typed%s(this.http.%s(%s'/%s'%s%s%s)%s, %s)\n",
                                        isArray(method.getReturnType()) ? "Array" : "",
                                        toHttpClientMethod(requestMethod),
                                        !urlParams.isEmpty() ? "this.httpUtils.pathEncode(" : "",
                                        servicePath,
                                        !urlParams.isEmpty() ? ", " + urlParams + ")" : "",
                                        requestBodyHelper.getRequestBody(),
                                        toParams(httpContext, httpParams, requestBodyHelper, null),
                                        addOptionalResultPipe(requestBodyHelper.withProgress()),
                                        resultForceOptionalProperties);
                    }
                }
            }
            writer.println("  }");
        }
        writer.println("}");

        if (writerInterface != null) {
            writerInterface.println("}");
            writerInterface.flush();
        }
        writer.println();
        writer.println(writerInterfaceBuffer);
    }

    protected List<ReqParameter> getParameters(Method method) {
        List<ReqParameter> ret = new ArrayList<>();

        for (Parameter parameter : method.getParameters()) {
            CustomType customType = typescriptGenerator.findCustomTypeFor(parameter.getType());
            if (customType != null) {
                List<ReqParameter> customParameter = customType.getRequestParameters(typescriptGenerator, this, parameter.getParameterizedType());
                if (customParameter != null) {
                    ret.addAll(customParameter);
                    continue;
                }
            }

            if (parameter.getType().isAnnotationPresent(ExportTypescript.class)
                    && parameter.getType().getAnnotation(ExportTypescript.class).embedded()) {
                for (Field field : getDeclaredFields(parameter.getType())) {
                    ReqParameter reqParameter = new ReqParameter(
                            field.getName(),
                            field.getType(),
                            field.getGenericType(),
                            Toolbox.addAll(field.getAnnotations(), defaultRequestParam(field.getName()), parameter.getType().getAnnotation(ExportTypescript.class)));
                    ret.add(reqParameter);
                }
                continue;
            }

            // usual parameter
            ReqParameter reqParameter = new ReqParameter(
                    parameter.getName(),
                    parameter.getType(),
                    parameter.getParameterizedType(),
                    parameter.getAnnotations());
            ret.add(reqParameter);
        }

        return ret;
    }

    private boolean isPrimitive(String tsType) {
        return "string".equals(tsType) || "number".equals(tsType) || "boolean".equals(tsType)
                || "string[]".equals(tsType) || "number[]".equals(tsType) || "boolean[]".equals(tsType);
    }

    @Override
    public Collection<String> tsImports(
            ExportTypescript exportTypescriptConfig, Class<?> type, Type genericType, boolean addI18n, boolean importAny) {
        CustomType customType = typescriptGenerator.findCustomTypeFor(type);
        Collection<String> result = super.tsImports(exportTypescriptConfig, type, genericType, addI18n, importAny);
        if (result.isEmpty() && customType != null) {
            Class<?> modelClass = customType.findBestMatchingDto(this, type, typescriptGenerator);

            if (modelClass != null) {
                result.add(String.format("import { %s } from '%s'",
                        typescriptGenerator.createName(modelClass),
                        typescriptGenerator.createFilename(modelClass, baseDir))
                );
            }
        }
        return result;
    }

    @Override
    public String tsType(String name, TypeInfo typeInfo, Type genericType, boolean withGenericsInfo, Map<String, String> genericsReplacement) {
        try {
            return super.tsType(name, typeInfo, genericType, withGenericsInfo, genericsReplacement);
        } catch (IllegalArgumentException e) {
            Class<?> type = typeInfo.getClazz();

            CustomType customType = typescriptGenerator.findCustomTypeFor(type);
            if (type != null && customType != null) {
                Class<?> modelClass = customType.findBestMatchingDto(this, type, typescriptGenerator);
                if (modelClass != null) {
                    return typescriptGenerator.createName(modelClass);
                }
            }

            throw e;
        }
    }

    private String buildHttpQueryParams(List<ReqParameter> reqParameters, Map<String, String> genericsReplacement) {
        StringBuilder sb = new StringBuilder();

        for (ReqParameter reqParameter : reqParameters) {
            Class<?> genericType = null;
            if (reqParameter.genericType instanceof ParameterizedType) {
                Type genericTypeType = ((ParameterizedType) reqParameter.genericType).getActualTypeArguments()[0];
                if (genericTypeType instanceof Class<?> genericTypeTypeClass) {
                    genericType = genericTypeTypeClass;
                } else if (genericTypeType instanceof ParameterizedType genericTypeTypeClass) {
                    genericType = (Class<?>) genericTypeTypeClass.getRawType();
                }
            }
            MergedAnnotation<RequestParam> requestParam = MergedAnnotations.from(reqParameter.annotations).get(RequestParam.class);
            if (!requestParam.isPresent()) {
                continue;
            }
            boolean required = requestParam.getBoolean("required");

            String name = Toolbox.defaultIfEmpty(requestParam.getString("name"), reqParameter.name);

            if (sb.isEmpty()) {
                sb.append("    const params = new HttpParamsBuilder(this.httpUtils)\n");
            }

            String type = tsType(reqParameter.name, new TypeInfo(reqParameter.type), null, true, genericsReplacement);

            String function = "append";
            if (!required) {
                function = "appendOpt";
            }
            sb.append(String.format("      .%s('%s', %s, %s)\n",
                    function,
                    name,
                    // TODO: implement real object to string conversion. e.g. also for date/time
                    getParameterConverter(reqParameter.name, type, reqParameter.type, genericType, required, true),
                    convertToJson(reqParameter.type, genericType) ? "true" : "false"));
        }

        if (!sb.isEmpty()) {
            sb.append("      .build()\n");
        }

        return sb.toString();
    }

    private String buildHttpContext(String service, String method, List<ReqParameter> reqParameters) {
        StringBuilder sb = new StringBuilder();

        sb.append(String.format("    const requestContext = new HttpRequestContext('%sService.%s')\n", service, method));

        for (ReqParameter reqParameter : reqParameters) {
            MergedAnnotations annotations = MergedAnnotations.from(reqParameter.annotations);

            MergedAnnotation<ExportTypescript> exportTypescript = annotations.get(ExportTypescript.class);
            if (exportTypescript.isPresent() && exportTypescript.getBoolean("ignore")) {
                continue;
            }

            MergedAnnotation<PathVariable> pathVariable = annotations.get(PathVariable.class);
            MergedAnnotation<RequestParam> requestParam = annotations.get(RequestParam.class);
            MergedAnnotation<RequestBody> requestBody = annotations.get(RequestBody.class);
            MergedAnnotation<RequestPart> requestPart = annotations.get(RequestPart.class);
            boolean required;
            String name = reqParameter.name;
            if (requestParam.isPresent()) {
                required = requestParam.getBoolean("required");
            } else if (pathVariable.isPresent()) {
                required = pathVariable.getBoolean("required");
            } else if (requestBody.isPresent()) {
                required = requestBody.getBoolean("required");
            } else if (requestPart.isPresent()) {
                required = requestPart.getBoolean("required");
            } else {
                continue;
            }

            String parsedParamName;
            if (required) {
                parsedParamName = name;
            } else {
                parsedParamName = String.format("options?.%s", name);
            }

            sb.append(String.format("      .set('%s', %s)\n", name, parsedParamName));
        }

        sb.append("    const context = new HttpContext()\n");
        sb.append("      .set(HTTP_CONTEXT_TOKEN, requestContext)\n");

        return sb.toString();
    }

    private String buildFormDataParams(List<ReqParameter> reqParameters, Map<String, String> genericsReplacement) {
        StringBuilder sb = new StringBuilder();

        for (ReqParameter reqParameter : reqParameters) {
            Class<?> genericType = null;
            if (reqParameter.genericType instanceof ParameterizedType) {
                Type genericTypeType = ((ParameterizedType) reqParameter.genericType).getActualTypeArguments()[0];
                if (genericTypeType instanceof Class<?> genericTypeTypeClass) {
                    genericType = genericTypeTypeClass;
                } else if (genericTypeType instanceof ParameterizedType genericTypeTypeClass) {
                    genericType = (Class<?>) genericTypeTypeClass.getRawType();
                }
            }
            MergedAnnotation<RequestPart> requestParam = MergedAnnotations.from(reqParameter.annotations).get(RequestPart.class);
            if (!requestParam.isPresent()) {
                continue;
            }
            boolean required = requestParam.getBoolean("required");

            String name = Toolbox.defaultIfEmpty(requestParam.getString("name"), reqParameter.name);

            if (sb.isEmpty()) {
                sb.append("    const formData = new HttpFormDataBuilder(this.httpUtils)\n");
            }

            String type = tsType(reqParameter.name, new TypeInfo(reqParameter.type), null, true, genericsReplacement);

            String function = "append";
            if (!required) {
                function = "appendOpt";
            }
            sb.append(String.format("      .%s('%s', %s)\n",
                    function,
                    name,
                    // TODO: implement real object to string conversion. e.g. also for date/time
                    getParameterConverter(reqParameter.name, type, reqParameter.type, genericType, required, false)));
        }

        if (!sb.isEmpty()) {
            sb.append("      .build()\n");
        }

        return sb.toString();
    }

    private String buildHttpUrlParams(List<ReqParameter> reqParameters, Map<String, String> genericsReplacement) {
        StringBuilder sb = new StringBuilder();
        for (ReqParameter reqParameter : reqParameters) {
            Class<?> genericType = null;
            if (reqParameter.genericType instanceof ParameterizedType) {
                Type genericTypeType = ((ParameterizedType) reqParameter.genericType).getActualTypeArguments()[0];
                if (genericTypeType instanceof Class<?> genericTypeTypeClass) {
                    genericType = genericTypeTypeClass;
                } else if (genericTypeType instanceof ParameterizedType parameterizedTypeClass) {
                    genericType = (Class<?>) parameterizedTypeClass.getRawType();
                }
            }

            MergedAnnotation<PathVariable> requestParam = MergedAnnotations.from(reqParameter.annotations).get(PathVariable.class);
            if (!requestParam.isPresent()) {
                continue;
            }

            boolean required = requestParam.getBoolean("required");

            if (sb.isEmpty()) {
                sb.append(" {\n");
            } else {
                sb.append(",\n");
            }

            String type = tsType(reqParameter.name, new TypeInfo(reqParameter.type), null, true, genericsReplacement);
            String name = Toolbox.defaultIfEmpty(requestParam.getString("name"), reqParameter.name);

            sb.append("      // tslint:disable-next-line:object-literal-shorthand\n");
            sb.append(String.format("      %s: %s",
                    name,
                    // TODO: implement real object to string conversion. e.g. also for date/time
                    getParameterConverter(reqParameter.name, type, reqParameter.type, genericType, required, true)));
        }

        if (!sb.isEmpty()) {
            sb.append("\n    }");
        }

        return sb.toString();
    }

    private boolean convertToJson(Class<?> parameterType, Class<?> genericType) {
        Class<?> componentType = Objects.requireNonNullElse(genericType, Objects.requireNonNullElse(parameterType.getComponentType(), parameterType));
        if (componentType.isAnnotationPresent(JsonTypeName.class)) {
            return true;
        }

        return !parameterType.isEnum()
                && parameterType.isAnnotationPresent(ExportTypescript.class)
                && parameterType.getAnnotation(ExportTypescript.class).buildToString().isEmpty();
    }

    protected String getParameterConverter(String parameterName, String type, Class<?> parameterType, Class<?> genericType, boolean required, boolean unwrapArray) {
        CustomType customType = typescriptGenerator.findCustomTypeFor(parameterType);
        Class<?> componentType = Objects.requireNonNullElse(genericType, Objects.requireNonNullElse(parameterType.getComponentType(), parameterType));
        String parsedParamName;
        if (required) {
            parsedParamName = parameterName;
        } else {
            parsedParamName = String.format("options?.%s", parameterName);
        }
        if (componentType.isAnnotationPresent(JsonTypeName.class)) {
            return String.format("%s", parsedParamName);
        }
        if (isArray(parameterType)) {
            if (!unwrapArray) {
                return String.format("%s", parsedParamName);
            }

            if (String.class == componentType) {
                return String.format("%s", parsedParamName);
            } else {
                return String.format("%s%s.map(val => %s)", parsedParamName, required ? "" : "?", getParameterConverter("val", type, componentType, null, true, true));
            }
        }
        if (Map.class.isAssignableFrom(parameterType) && !unwrapArray) {
            return String.format("%s", parsedParamName);
        }
        if (customType != null) {
            return customType.asParameter(typescriptGenerator, this, parameterType, parsedParamName, required, typescriptGenerator);
        }
        if ("string".equals(type)) {
            return parsedParamName;
        }
        if (parameterType.isEnum()) {
            return parsedParamName;
        }
        if (parameterType.isAnnotationPresent(ExportTypescript.class)
                && parameterType.getAnnotation(ExportTypescript.class).buildToString().isEmpty()) {
            return String.format("%s", parsedParamName);
        }

        return String.format("%s%s.toString()", parsedParamName, required ? "" : "?");
    }

    public void writeArguments(Method method, PrintWriter writer, List<ReqParameter> reqParameters, Map<String, String> genericsReplacement, boolean withProgress) {
        StringBuilder requiredArguments = new StringBuilder();
        StringBuilder optionalArguments = new StringBuilder();

        for (ReqParameter reqParameter : reqParameters) {
            Type type = reqParameter.genericType;

            ExportAs exportAs = findAnnotation(reqParameter.annotations, ExportAs.class);

            MergedAnnotations annotations = MergedAnnotations.from(reqParameter.annotations);
            MergedAnnotation<PathVariable> pathVariable = annotations.get(PathVariable.class);
            MergedAnnotation<RequestParam> requestParam = annotations.get(RequestParam.class);
            MergedAnnotation<RequestBody> requestBody = annotations.get(RequestBody.class);
            MergedAnnotation<RequestPart> requestPart = annotations.get(RequestPart.class);

            if (requestBody.isPresent()) {
                if (requestBody.getBoolean("required")) {
                    addRequired(method, requiredArguments, reqParameter, type, exportAs, genericsReplacement);
                } else {
                    addOptional(method, optionalArguments, reqParameter, type, exportAs, genericsReplacement);
                }
            } else if (pathVariable.isPresent()) {
                if (pathVariable.getBoolean("required")) {
                    addRequired(method, requiredArguments, reqParameter, type, exportAs, genericsReplacement);
                } else {
                    addOptional(method, optionalArguments, reqParameter, type, exportAs, genericsReplacement);
                }
            } else if (requestParam.isPresent()) {
                if (requestParam.getBoolean("required")) {
                    addRequired(method, requiredArguments, reqParameter, type, exportAs, genericsReplacement);
                } else {
                    addOptional(method, optionalArguments, reqParameter, type, exportAs, genericsReplacement);
                }
            } else if (requestPart.isPresent()) {
                if (requestPart.getBoolean("required")) {
                    addRequired(method, requiredArguments, reqParameter, type, exportAs, genericsReplacement);
                } else {
                    addOptional(method, optionalArguments, reqParameter, type, exportAs, genericsReplacement);
                }
            }
        }

        if (withProgress) {
            addOptionalDirect(optionalArguments, "progress", "Subject<HttpProgressEvent>");
        }

        boolean limitExceeded = requiredArguments.length() > 100;

        if (!optionalArguments.isEmpty()) {
            optionalArguments.append("  }");
        }

        if (!requiredArguments.isEmpty()) {
            if (!limitExceeded) {
                writer.print(requiredArguments.toString().replaceAll("\\s+", " "));
            } else {
                writer.print("\n    ");
                writer.print(requiredArguments);
            }
            if (!optionalArguments.isEmpty()) {
                if (limitExceeded) {
                    writer.print(",\n    ");
                } else {
                    writer.print(", ");
                }
            } else if (limitExceeded) {
                writer.print("\n  ");
            }
        }
        if (!optionalArguments.isEmpty()) {
            if (limitExceeded) {
                writer.print(optionalArguments.toString().replaceAll("[\\s&&[^\\n]]{4}", "      "));
            } else {
                writer.print(optionalArguments);
            }
        }
    }

    private void addRequired(Method method, StringBuilder buffer, ReqParameter reqParameter, Type type, ExportAs exportAs, Map<String, String> genericsReplacement) {
        if (!buffer.isEmpty()) {
            buffer.append(",\n    ");
        }
        buffer.append(reqParameter.name);
        buffer.append(": ");
        buffer.append(addParameterType(method, reqParameter, type, exportAs, genericsReplacement));
    }

    private void addOptional(Method method, StringBuilder buffer, ReqParameter reqParameter, Type type, ExportAs exportAs, Map<String, String> genericsReplacement) {
        addOptionalDirect(buffer,
                reqParameter.name,
                addParameterType(method, reqParameter, type, exportAs, genericsReplacement));
    }

    private void addOptionalDirect(StringBuilder buffer, String name, String type) {
        if (buffer.isEmpty()) {
            buffer.append("options?: {\n");
        }
        buffer.append("    ");
        buffer.append(name);
        buffer.append("?: ");
        buffer.append(type);
        buffer.append(";\n");
    }

    private String addParameterType(Method method, ReqParameter reqParameter, Type type, ExportAs exportAs, Map<String, String> genericsReplacement) {
        if (exportAs == null || (exportAs.type() == void.class && ObjectUtils.isEmpty(exportAs.tsType()))) {
            return tsType(reqParameter.name, new TypeInfo(reqParameter.type), type, true, genericsReplacement);
        } else if (!ObjectUtils.isEmpty(exportAs.tsType())) {
            return convertExportAsType(method, exportAs);
        } else if (exportAs.type() != void.class) {
            return tsType(reqParameter.name, new TypeInfo(exportAs.type()), exportAs.genericType(), true, null);
        }

        throw new IllegalArgumentException("Can't determine parameter type for " + method.getName() + "#" + reqParameter.name);
    }

    @NotNull
    private String convertExportAsType(Method method, ExportAs exportAs) {
        String exportAsType = exportAs.tsType();
        Type returnType = method.getGenericReturnType();
        CustomType customType = typescriptGenerator.findCustomTypeFor(method.getReturnType());
        if (customType != null) {
            returnType = findClass(method.getGenericReturnType(), customType.getType());
        } else if (List.class.isAssignableFrom(method.getReturnType())) {
            returnType = findClass(method.getGenericReturnType(), List.class);
        }
        TypeInfo returnTypeInfo;
        if (returnType instanceof ParameterizedType) {
            returnTypeInfo = getType(returnType, 0);
        } else {
            returnTypeInfo = new TypeInfo((Class<?>) returnType);
        }

        String entityReturnType = tsType("method return", returnTypeInfo, returnType, true, null);
        exportAsType = exportAsType.replace("$ENTITY", entityReturnType);
        return exportAsType;
    }

    private Type findClass(Type base, Class<?> classToSearch) {
        Type search = base;
        do {
            if (search instanceof ParameterizedType) {
                if (((ParameterizedType) search).getRawType() == classToSearch) {
                    return search;
                }
                search = ((Class<?>) ((ParameterizedType) search).getRawType()).getSuperclass();
            } else if (search instanceof Class) {
                if (classToSearch == base) {
                    return base;
                }
                search = ((Class<?>) search).getGenericSuperclass();
            } else {
                throw new IllegalArgumentException("unknown type: " + search.getClass().getName());
            }
        }
        while (search != null);

        return null;
    }

    public boolean isArray(Class<?> type) {
        return List.class.isAssignableFrom(type)
                || Set.class.isAssignableFrom(type)
                || type.isArray();
    }

    public Object toHttpClientMethod(RequestMethod requestMethod) {
        switch (requestMethod) {
            case GET -> {
                return "get";
            }
            case POST -> {
                return "post";
            }
            case PATCH -> {
                return "patch";
            }
            case PUT -> {
                return "put";
            }
            case DELETE -> {
                return "delete";
            }
        }

        throw new IllegalArgumentException("unknown method: " + requestMethod);
    }

    public String toParams(String httpContext, String httpParams, RequestBodyHelper requestBodyHelper, String withResponseType) {
        if (!requestBodyHelper.withProgress() && requestBodyHelper.getOptionRequestBody() == null && httpContext.isEmpty() && httpParams.isEmpty() && StringUtils.isBlank(withResponseType)) {
            return "";
        } else {
            boolean hasParam = false;
            StringBuilder sb = new StringBuilder();

            sb.append(", {");

            if (!httpContext.isEmpty()) {
                sb.append("context");
                hasParam = true;
            }
            if (!httpParams.isEmpty()) {
                if (hasParam) {
                    sb.append(", ");
                }
                sb.append("params");
                hasParam = true;
            }
            if (requestBodyHelper.withProgress()) {
                if (hasParam) {
                    sb.append(", ");
                }
                sb.append("observe: 'events', reportProgress: true");
                hasParam = true;
            }
            if (requestBodyHelper.getOptionRequestBody() != null) {
                if (hasParam) {
                    sb.append(", ");
                }
                sb.append(requestBodyHelper.getOptionRequestBody());
                hasParam = true;
            }
            if (withResponseType != null) {
                if (hasParam) {
                    sb.append(", ");
                }
                sb.append("responseType: '");
                sb.append(withResponseType);
                sb.append("'");
                hasParam = true;
            }

            sb.append("}");

            return sb.toString();
        }
    }

    public String addOptionalResultPipe(boolean withProgress) {
        if (!withProgress) {
            return "";
        } else {
            return """
                    .pipe(
                            tap(httpEvent => httpEvent.type === HttpEventType.UploadProgress ? options?.progress?.next(httpEvent as HttpProgressEvent) : undefined),
                            filter(httpEvent => (httpEvent.type === HttpEventType.Response)),
                            map(httpEvent => httpEvent as HttpResponse<Object>),
                            map(httpResponse => httpResponse.body)
                          )""";
        }
    }

    public static class RequestBodyHelper {
        private final String requestBody;
        private final String optionRequestBody;
        private final String requestBodyWrapper;
        private final boolean needsProgress;

        private RequestBodyHelper(RequestMethod requestMethod, Method method) {
            //Class<?> requestBodyClass = null;
            String requestBody = "";
            String optionRequestBody = null;
            String requestBodyWrapper = null;
            boolean needsProgress = false;
            if (requestMethod == RequestMethod.PUT
                    || requestMethod == RequestMethod.POST
                    || requestMethod == RequestMethod.PATCH
                    || requestMethod == RequestMethod.DELETE) {
                int requestBodyParam = findRequestBody(method.getParameterAnnotations());
                if (hasRequestPart(method.getParameterAnnotations())) {
                    if (requestMethod == RequestMethod.DELETE) {
                        requestBody = "";
                        optionRequestBody = "body: formData";
                    } else {
                        requestBody = ", formData";
                    }
                } else if (requestBodyParam > -1) {
                    Class<?> requestBodyClass = method.getParameters()[requestBodyParam].getType();
                    needsProgress = needsProgress(requestBodyClass);

                    String parameterName = method.getParameters()[requestBodyParam].getName();

                    String parameterPath;
                    if (isRequestBodyOptional(method.getParameterAnnotations())) {
                        parameterPath = parameterName;
                    } else {
                        parameterPath = "options?." + parameterName;
                    }

                    String requestBodyVariablePath;
                    if (needsWrapper(requestBodyClass)) {
                        String wrapperName = parameterName + "Wrapper";
                        requestBodyWrapper = buildWrapper(requestBodyClass, wrapperName, parameterName, parameterPath);
                        requestBodyVariablePath = wrapperName;
                    } else {
                        requestBodyVariablePath = parameterPath;
                    }

                    if (requestMethod == RequestMethod.DELETE) {
                        requestBody = "";
                        optionRequestBody = "body: " + requestBodyVariablePath;
                    } else {
                        requestBody = ", " + requestBodyVariablePath;
                    }
                } else if (requestMethod != RequestMethod.DELETE) {
                    requestBody = ", null";
                }
            }

            this.requestBody = requestBody;
            this.optionRequestBody = optionRequestBody;
            this.requestBodyWrapper = requestBodyWrapper;
            this.needsProgress = needsProgress;
        }

        public String getRequestBody() {
            return requestBody;
        }

        public String getOptionRequestBody() {
            return optionRequestBody;
        }

        public boolean hasRequestBodyWrapper() {
            return requestBodyWrapper != null;
        }

        public String getRequestBodyWrapper() {
            return requestBodyWrapper;
        }

        public boolean withProgress() {
            return needsProgress;
        }

        private boolean needsWrapper(Class<?> requestBodyClass) {
            return MultipartFile.class.equals(requestBodyClass);
        }

        private boolean needsProgress(Class<?> requestBodyClass) {
            return MultipartFile.class.equals(requestBodyClass);
        }

        private String buildWrapper(Class<?> requestBodyClass, String wrapperName, String paramName, String paramPath) {
            if (MultipartFile.class.equals(requestBodyClass)) {
                return String.format("    const %s: FormData = new FormData()\n" +
                        "      %s.append('%s', %s, %s.name)", wrapperName, wrapperName, paramName, paramPath, paramPath);
            } else {
                return null;
            }
        }

        private boolean hasRequestPart(Annotation[][] parameterAnnotations) {
            if (parameterAnnotations == null) {
                return false;
            }
            for (int i = 0; i < parameterAnnotations.length; i++) {
                MergedAnnotation<RequestPart> requestParam = MergedAnnotations.from(parameterAnnotations[i]).get(RequestPart.class);
                if (requestParam.isPresent()) {
                    return true;
                }
            }
            return false;
        }

        private int findRequestBody(Annotation[][] parameterAnnotations) {
            if (parameterAnnotations == null) {
                return -1;
            }
            for (int i = 0; i < parameterAnnotations.length; i++) {
                MergedAnnotation<RequestBody> requestParam = MergedAnnotations.from(parameterAnnotations[i]).get(RequestBody.class);
                if (requestParam.isPresent()) {
                    return i;
                }
            }
            return -1;
        }

        private boolean isRequestBodyOptional(Annotation[][] parameterAnnotations) {
            if (parameterAnnotations == null) {
                return false;
            }
            for (Annotation[] parameterAnnotation : parameterAnnotations) {
                MergedAnnotation<RequestBody> requestParam = MergedAnnotations.from(parameterAnnotation).get(RequestBody.class);
                if (requestParam.isPresent()) {
                    return requestParam.getBoolean("required");
                }
            }

            return false;
        }
    }
}
