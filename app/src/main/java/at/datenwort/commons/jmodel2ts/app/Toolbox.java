/*
 * Copyright (c) 2022 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package at.datenwort.commons.jmodel2ts.app;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.util.ObjectUtils;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

final class Toolbox {
    private Toolbox() {
    }

    @Contract("_, null -> null")
    public static String join(@NotNull String delimiter,
                              @Nullable Collection<?> collection) {
        if (collection == null) {
            return null;
        }

        StringBuilder sb = new StringBuilder();
        for (Object element : collection) {
            if (ObjectUtils.isEmpty(element)) {
                continue;
            }

            if (sb.length() > 0) {
                sb.append(delimiter);
            }

            sb.append(element);
        }

        return sb.toString();
    }

    public static <T> T getFirstNonNull(T[] elements) {
        if (elements == null) {
            return null;
        }

        for (T element : elements) {
            if (element != null) {
                return element;
            }
        }

        return null;
    }

    public static String joinPath(String... pathElements) {
        if (ObjectUtils.isEmpty(pathElements)) {
            return null;
        }

        final String delimiter = "/";
        StringBuilder sb = new StringBuilder();
        for (String pathElement : pathElements) {
            if (pathElement == null) {
                continue;
            }

            if (sb.length() > 0 && !sb.toString().endsWith(delimiter)) {
                sb.append(delimiter);
            }

            if (pathElement.startsWith(delimiter)) {
                pathElement = pathElement.substring(delimiter.length());
            }

            sb.append(pathElement);
        }

        return sb.toString();
    }

    public static String defaultIfEmpty(String value, String def) {
        if (!ObjectUtils.isEmpty(value)) {
            return value;
        }
        return def;
    }

    @SafeVarargs
    public static <T> T[] addAll(@NotNull T[] array, T... array2) {
        if (array2 == null || array2.length == 0) {
            return array;
        }

        List<T> ret = new ArrayList<>();
        ret.addAll(Arrays.asList(array));
        ret.addAll(Arrays.asList(array2));
        //noinspection unchecked
        return ret.toArray((T[]) Array.newInstance(array.getClass().getComponentType(), 0));
    }
}
