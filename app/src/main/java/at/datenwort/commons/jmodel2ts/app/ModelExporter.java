/*
 * Copyright (c) 2022 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package at.datenwort.commons.jmodel2ts.app;

import at.datenwort.commons.jmodel2ts.annotations.ExportAs;
import at.datenwort.commons.jmodel2ts.annotations.ExportTypescript;
import at.datenwort.commons.jmodel2ts.annotations.TsImport;
import at.datenwort.commons.jmodel2ts.annotations.TsMethod;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.springframework.util.ObjectUtils;

import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class ModelExporter extends AbstractModelExporter {

    public ModelExporter(TypescriptGenerator typescriptGenerator, Class<?> clazz, String name, String model) {
        super(typescriptGenerator, clazz, name, model);
    }

    public void export(PrintWriter writer) {
        ExportTypescript exportTypescript = getAnnotation(clazz, ExportTypescript.class);
        JsonTypeInfo jsonTypeInfo = getAnnotation(clazz, JsonTypeInfo.class);
        JsonSubTypes jsonSubTypes = getAnnotation(clazz, JsonSubTypes.class);

        writer.println("// noinspection ES6UnusedImports");
        writer.println("import { JsonObject, JsonProperty } from '@jmodel2ts/json2typescript/json-convert-decorators'");
        if (typescriptGenerator.isGenerateRuntimeInfo()) {
            writer.println("import { FieldDefinition, InstanceDefinition } from '@jmodel2ts/api/runtime'");
        }
        writer.println("import { createProxy } from '@jmodel2ts/api/ts-object-path'");
        if (exportTypescript.generateTeplateDirectives()) {
            writer.println("import { SelectionListItemContext, SelectionListItemDirective } from '@shared/modules/ui/selection-list/selection-list-item.directive'");
            writer.println("import { SelectOptionContext, SelectOptionDirective } from '@shared/modules/forms/controls/select/select.component'");
            writer.println("import { Directive, forwardRef, TemplateRef } from '@angular/core'");
        }

        Set<String> imports = new TreeSet<>();

        List<Method> declaredMethods = getDeclaredMethods(clazz);
        for (Method method : declaredMethods) {
            ExportTypescript methodConfig = method.getAnnotation(ExportTypescript.class);
            if (methodConfig == null) {
                continue;
            }

            if (ObjectUtils.isEmpty(methodConfig.methodBody())) {
                continue;
            }

            imports.addAll(tsImports(
                    methodConfig,
                    method.getReturnType(), method.getGenericReturnType(), false, true));
        }

        List<Field> declaredFields = getDeclaredFields(clazz);
        for (Field field : declaredFields) {
            if (Modifier.isStatic(field.getModifiers())
                    || Modifier.isTransient(field.getModifiers())) {
                continue;
            }
            if (field.getType() == clazz
                    || field.getType().getComponentType() == clazz
                    || getType(field.getGenericType(), 0).getClazz() == clazz) {
                continue;
            }

            imports.addAll(tsImports(
                    field.getAnnotation(ExportTypescript.class),
                    field.getType(), field.getGenericType(), false, true));

            JsonSubTypes fieldSubTypes = field.getAnnotation(JsonSubTypes.class);
            if (fieldSubTypes != null) {
                imports.add("import { TypedConverter } from '@jmodel2ts/json2typescript/common-json-converter'");

                Arrays.stream(fieldSubTypes.value())
                        .map(JsonSubTypes.Type::value)
                        .forEach(type -> imports.addAll(tsImports(
                                field.getAnnotation(ExportTypescript.class),
                                type, null, false, false)));
            }
        }

        if (clazz.getSuperclass().isAnnotationPresent(ExportTypescript.class)) {
            imports.addAll(tsImports(null, clazz.getSuperclass(), null, exportTypescript.i18n(), true));

            // TODO: should be recursive
            Type type = clazz.getAnnotatedSuperclass().getType();
            if (type instanceof ParameterizedType parameterizedType) {
                for (Type boundsType : parameterizedType.getActualTypeArguments()) {
                    imports.addAll(tsImports(null, (Class<?>) boundsType, null, false, true));
                }
            }
        }
        for (Class<?> implementedInterface : clazz.getInterfaces()) {
            if (implementedInterface.isAnnotationPresent(ExportTypescript.class)) {
                imports.addAll(tsImports(null, implementedInterface, null, false, true));
            }
        }

        imports.stream()
                .filter(Objects::nonNull)
                .forEach(writer::println);

        for (TsImport tsImport : exportTypescript.imports()) {
            writer.println(tsImport.value());
        }

        boolean hasGenerics = clazz.getTypeParameters().length > 0;
        boolean hasParent = clazz.getSuperclass().isAnnotationPresent(ExportTypescript.class);
        String extendsClass;
        if (hasParent) {
            extendsClass = typescriptGenerator.createName(clazz.getSuperclass());
        } else {
            extendsClass = null;
        }

        writer.println();
        if (typescriptGenerator.isGenerateRuntimeInfo()) {
            if (hasParent) {
                writer.format("@InstanceDefinition(%s, %s, %s)\n", constant(clazz.getName()), constant(className), constant(extendsClass));
            } else {
                writer.format("@InstanceDefinition(%s, %s)\n", constant(clazz.getName()), constant(className));
            }
        }
        writer.format("@JsonObject(%s)\n", constant(className));
        writer.format("export%s class %s",
                Modifier.isAbstract(clazz.getModifiers()) ? " abstract" : "",
                className);

        List<String> anyAnyGenerics = new ArrayList<>();
        List<String> fieldNames = new ArrayList<>();
        StringBuilder typeStringBuilder = new StringBuilder();
        for (TypeVariable<? extends Class<?>> typeVariable : clazz.getTypeParameters()) {
            if (typeStringBuilder.isEmpty()) {
                typeStringBuilder.append("<");
            } else {
                typeStringBuilder.append(", ");
            }
            typeStringBuilder.append(typeVariable.getName());
            anyAnyGenerics.add("any");
        }

        if (!typeStringBuilder.isEmpty()) {
            typeStringBuilder.append(">");

            writer.print(typeStringBuilder);
        }

        writer.print(" ");

        String override = "";

        //if(clazz.getgenericInfo)
        if (hasParent) {
            override = "override ";

            writer.format(" extends %s", extendsClass);

            Type type = clazz.getAnnotatedSuperclass().getType();
            if (type instanceof ParameterizedType parameterizedType) {

                writer.format("<");
                boolean first = true;
                for (Type boundsType : parameterizedType.getActualTypeArguments()) {
                    if (!first) {
                        writer.print(", ");
                    }

                    Class<?> boundsClass = (Class<?>) boundsType;
                    writer.print(tsType(clazz.getSuperclass().getName(), new TypeInfo(boundsClass), null, true, null));

                    first = false;
                }
                writer.format(">");
            }

            writer.print(" ");
        }

        boolean implementsAdded = false;
        for (Class<?> extendsInterface : clazz.getInterfaces()) {
            if (extendsInterface.isAnnotationPresent(ExportTypescript.class)) {
                String implementsInterfaceName = typescriptGenerator.createName(extendsInterface);

                if (!implementsAdded) {
                    implementsAdded = true;
                    writer.print(" implements ");
                } else {
                    writer.print(", ");
                }
                writer.format(" %s", implementsInterfaceName);
            }
        }
        if (implementsAdded) {
            writer.print(" ");
        }

        writer.println("{");

        boolean addedImplementsSignature = false;
        Set<Class<?>> implementsSeen = new HashSet<>();
        for (ImplementedInterface implementedInterface : implementedInterfaces(clazz)) {
            if (implementsSeen.contains(implementedInterface.iface)) {
                continue;
            }
            if (!implementedInterface.iface.isAnnotationPresent(ExportTypescript.class)) {
                continue;
            }

            addedImplementsSignature = true;
            writer.format("  public static %sreadonly _implements_%s = true\n",
                    implementedInterface.superclass ? "override " : "",
                    implementedInterface.iface.getName().replace(".", "_"));
            implementsSeen.add(implementedInterface.iface);
        }

        if (addedImplementsSignature) {
            writer.println();
        }
        if (typescriptGenerator.getLocalizationType() == LocalizationType.ANGULAR_LOCALIZE) {
            writer.format("  public %sreadonly lng = %sI18n\n", override, className);
        }
        writer.format("  public static %sreadonly p = createProxy<%s%s>('%s', '%s')\n",
                override,
                className,
                !anyAnyGenerics.isEmpty() ? "<" + Toolbox.join(",", anyAnyGenerics) + ">" : "",
                clazz.getName(),
                Character.toLowerCase(className.charAt(0)) + className.substring(1)
        );
        writer.println();

        if (jsonTypeInfo != null && jsonSubTypes != null) {
            JsonSubTypes.Type jsonType = Arrays.stream(jsonSubTypes.value())
                    .filter(type -> type.value() == clazz)
                    .findFirst()
                    .orElse(null);
            if (jsonType != null) {
                if (typescriptGenerator.isGenerateRuntimeInfo()) {
                    writer.println(String.format("  @FieldDefinition(%s, %s, %s, false)",
                            constant(clazz.getName()),
                            constant(jsonTypeInfo.property()),
                            constant("String")));
                }

                writer.print("  @JsonProperty(");
                writer.print(constant(jsonTypeInfo.property()));
                writer.println(", String)");
                writer.print("  readonly ");
                writer.print(jsonTypeInfo.property());
                writer.print("='");
                writer.print(jsonType.name());
                writer.println("'");
                writer.println();
            }
        }
        if (exportTypescript.constructor().render()) {
            int count = 0;

            writer.println();
            writer.print("  constructor(");
            for (Field field : declaredFields) {
                if (Modifier.isStatic(field.getModifiers())
                        || Modifier.isTransient(field.getModifiers())) {
                    continue;
                }

                if (count > 0) {
                    writer.print(", ");
                }

                String fieldName = field.getName();

                String tsType = getTsType(field);

                if (exportTypescript.constructor().optional()) {
                    writer.format("%s?: %s", fieldName, tsType);
                } else {
                    writer.format("%s: %s", fieldName, tsType);
                }

                count++;
            }

            writer.println(") {");

            for (Field field : declaredFields) {
                if (Modifier.isStatic(field.getModifiers())
                        || Modifier.isTransient(field.getModifiers())) {
                    continue;
                }

                String fieldName = field.getName();

                writer.format("    this.%s = %s", fieldName, fieldName);
                writer.println();
                count++;
            }

            writer.println("  }");
            writer.println();
        }

        int count = 0;
        for (Field field : declaredFields) {
            if (field.getName().startsWith("ajc$")) {
                continue;
            }

            if (Modifier.isStatic(field.getModifiers())
                    || Modifier.isTransient(field.getModifiers())) {
                continue;
            }
            ExportTypescript fieldSettings = field.getAnnotation(ExportTypescript.class);
            if (fieldSettings != null && fieldSettings.ignore()) {
                continue;
            }

            if (count > 0) {
                writer.println();
            }

            String fieldName = field.getName();

            String tsType = getTsType(field);

            copyDoc(writer, field);

            if (typescriptGenerator.isGenerateRuntimeInfo()) {
                writer.println(String.format("  @FieldDefinition(%s, %s, %s, %s)",
                        constant(clazz.getName()),
                        constant(fieldName),
                        constant(getComponentType(new TypeInfo(field.getType(), field.getGenericType()))),
                        tsType.endsWith("[]") ? "true" : "false"
                ));
            }

            JsonSubTypes fieldSubTypes = field.getAnnotation(JsonSubTypes.class);
            if (fieldSubTypes != null) {
                String tsSubTypeTypes = Arrays.stream(fieldSubTypes.value())
                        .map(JsonSubTypes.Type::value)
                        .map(type -> jsonType(field.getAnnotation(ExportTypescript.class),
                                new TypeInfo(type),
                                null,
                                false,
                                false))
                        .sorted()
                        .distinct()
                        .collect(Collectors.joining(" | "));

                writer.format("  @JsonProperty(%s, %s%s)\n",
                        constant(fieldName),
                        "TypedConverter",
                        isRequired(field, field.getType()) ? "" : ", true"
                );
                writer.println("    // @ts-ignore");
                writer.format("  %s%s: %s = undefined\n",
                        fieldName,
                        isRequired(field, field.getType()) ? "" : "?",
                        tsSubTypeTypes
                );

            } else {
                writer.format("  @JsonProperty(%s, %s%s)\n",
                        constant(fieldName),
                        jsonType(field.getAnnotation(ExportTypescript.class),
                                new TypeInfo(field.getType()), field.getGenericType(),
                                false,
                                typescriptGenerator.isJsonPropertyModelClassAsString()),
                        isRequired(field, field.getType()) ? "" : ", true"
                );
                writer.println("    // @ts-ignore");
                writer.format("  %s%s: %s = undefined\n",
                        fieldName,
                        isRequired(field, field.getType()) ? "" : "?",
                        tsType);
            }
            fieldNames.add(fieldName);

            count++;
        }

        if (!ObjectUtils.isEmpty(exportTypescript.buildToString())) {
            writer.println();
            writer.format("  %stoString(): string {\n", override);
            writer.println("    " + exportTypescript.buildToString());
            writer.println("  }");
        }

        for (TsMethod method : exportTypescript.methods()) {
            writer.println();
            writer.println(method.value());
        }

        if (!exportTypescript.constructor().render()
                && !Modifier.isAbstract(clazz.getModifiers())) {
            writer.println();
            writer.format("  constructor(data: Partial<%s%s> = {}) {\n", className, typeStringBuilder);
            if (clazz.getSuperclass().isAnnotationPresent(ExportTypescript.class)) {
                writer.println("    super()");
            }
            writer.println("    Object.assign(this, data)");
            writer.println("  }");
        }

        for (Method method : declaredMethods) {
            ExportTypescript methodConfig = method.getAnnotation(ExportTypescript.class);
            if (methodConfig == null) {
                continue;
            }

            if (ObjectUtils.isEmpty(methodConfig.methodBody())) {
                continue;
            }

            writer.println();

            copyDoc(writer, method);
            writer.println(String.format("  get %s(): %s {",
                    methodToFieldName(method.getName()),
                    tsType(method.getName(), new TypeInfo(method.getReturnType()), method.getGenericReturnType(), true, null)));
            writer.print("    ");
            writer.println(methodConfig.methodBody());
            writer.println("  }");
        }

        writer.println("}");

        if (!hasGenerics) {
            // TODO: work out for types with generics

            writer.println();
            writer.println(String.format("""               
                    export function instanceOf%s(value: any): value is %s {
                      return value instanceof %s
                    }
                    """, className, className, className));

            writer.println();
            writer.println(String.format("""               
                    export function if%s(value: any): %s | undefined {
                      return value instanceof %s ? value : undefined
                    }
                    """, className, className, className));
        }

        writer.println();

        if (typescriptGenerator.getLocalizationType() == LocalizationType.ANGULAR_LOCALIZE) {
            writer.format("export class %sI18n %s{\n", className,
                    extendsClass != null ? "extends " + extendsClass + "I18n " : "");
            for (String fieldName : fieldNames) {
                writer.format("  public static readonly l%s = $localize`:@@optionals.%s.%s:` || $localize`:@@globals.properties.%s:%s`\n",
                        Character.toUpperCase(fieldName.charAt(0)) + fieldName.substring(1), className, fieldName,
                        fieldName, fieldName);
            }
            writer.println("}");
        }

        if (exportTypescript.generateTeplateDirectives()) {
            writer.println();
            writer.println("@Directive({");
            writer.println(String.format("    selector: '[app%sSelectionListItem]',", className));
            writer.println("    standalone: true,");
            writer.println("    providers: [{");
            writer.println("        provide: SelectionListItemDirective,");
            writer.println(String.format("        useExisting: forwardRef(() => %sSelectionItemDirective)", className));
            writer.println("    }]");
            writer.println("})");
            writer.println(String.format("export class %sSelectionItemDirective extends SelectionListItemDirective<%s> {", className, className));
            writer.println(String.format("    constructor(public override template: TemplateRef<SelectionListItemContext<%s>>) {", className));
            writer.println("        super(template)");
            writer.println("    }");
            writer.println("}");
            writer.println();
            writer.println("@Directive({");
            writer.println(String.format("    selector: '[app%sSelectOption]',", className));
            writer.println("    standalone: true,");
            writer.println("    providers: [{");
            writer.println("        provide: SelectOptionDirective,");
            writer.println(String.format("        useExisting: forwardRef(() => %sSelectOptionDirective)", className));
            writer.println("    }]");
            writer.println("})");
            writer.println(String.format("export class %sSelectOptionDirective extends SelectOptionDirective<%s> {", className, className));
            writer.println(String.format("    constructor(public override template: TemplateRef<SelectOptionContext<%s>>) {", className));
            writer.println("        super(template)");
            writer.println("    }");
            writer.println("}");
        }

        writer.println();

        if (typescriptGenerator.getLocalizationType() == LocalizationType.ANGULAR_LOCALIZE) {
            writer.format("(%s as any).lng = %sI18n\n", className, className);
        }
    }

    private void copyDoc(PrintWriter writer, Method method) {
        List<String> docs = collectDoc(method);

        copyDoc(writer, docs, () -> {
            if (typescriptGenerator.getAdditionalDocBuilder() != null) {
                return typescriptGenerator.getAdditionalDocBuilder().apply(method);
            } else {
                return List.of();
            }
        });
    }

    private void copyDoc(PrintWriter writer, Field field) {
        List<String> docs = collectDoc(field);

        copyDoc(writer, docs, () -> {
            if (typescriptGenerator.getAdditionalDocBuilder() != null) {
                return typescriptGenerator.getAdditionalDocBuilder().apply(field);
            } else {
                return List.of();
            }
        });
    }

    private void copyDoc(PrintWriter writer, List<String> docs, Supplier<List<String>> additionalDocSupplier) {
        List<String> additionalDocs = additionalDocSupplier != null ? additionalDocSupplier.get() : List.of();

        if (!docs.isEmpty() || !additionalDocs.isEmpty()) {
            writer.println("  /**");
            for (String doc : docs) {
                writer.print("   * ");
                writer.println(doc);
            }
            for (String additionalDoc : additionalDocs) {
                if (!docs.isEmpty()) {
                    writer.print("   * <p>-</p>\n");
                }
                writer.print("   * ");
                writer.println(additionalDoc);
            }
            writer.println("   */");
        }
    }


    private String getComponentType(TypeInfo typeInfo) {
        if (typeInfo.getGenericSubType() instanceof Class<?> subtypeClass) {
            if (Collection.class.isAssignableFrom(subtypeClass)) {
                String ret = getComponentType(getType(typeInfo.getGenericSubType(), 0));
                return ret;
            }

            return subtypeClass.getName();
        }

        if (typeInfo.getGenericSubType() != null) {
            String ret = getComponentType(getType(typeInfo.getGenericSubType(), 0));
            return ret;
        } else {
            if (typeInfo.getClazz() == null) {
                return "undefined";
            }

            String ret = typeInfo.getClazz().getName();
            return ret;
        }
    }

    public static class ImplementedInterface {
        public final Class<?> iface;
        public final boolean superclass;

        public ImplementedInterface(Class<?> iface, boolean superclass) {
            this.iface = iface;
            this.superclass = superclass;
        }
    }

    private Collection<ImplementedInterface> implementedInterfaces(Class<?> clazz) {
        Set<ImplementedInterface> ret = new LinkedHashSet<>();
        implementedInterfaces(ret, clazz, false);
        return ret;
    }

    private void implementedInterfaces(Collection<ImplementedInterface> ret, Class<?> clazz, boolean superclass) {
        if (clazz == null) {
            return;
        }

        for (Class<?> iface : clazz.getInterfaces()) {
            ret.add(new ImplementedInterface(iface, superclass));

            implementedInterfaces(ret, iface, superclass);
        }

        implementedInterfaces(ret, clazz.getSuperclass(), true);
    }

    private String getTsType(Field field) {
        String tsType = null;
        if (field.isAnnotationPresent(ExportAs.class)) {
            ExportAs exportAs = field.getAnnotation(ExportAs.class);
            if (!ObjectUtils.isEmpty(exportAs.tsType())) {
                tsType = exportAs.tsType();
            }
        }
        if (tsType == null) {
            tsType = tsType(field.getName(), new TypeInfo(field.getType()), field.getGenericType(), true, null);
        }
        return tsType;
    }

}
