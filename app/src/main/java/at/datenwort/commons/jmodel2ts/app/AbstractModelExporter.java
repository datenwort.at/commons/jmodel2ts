/*
 * Copyright (c) 2022 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package at.datenwort.commons.jmodel2ts.app;

import at.datenwort.commons.jmodel2ts.annotations.ExportTypescript;
import at.datenwort.commons.jmodel2ts.api.CustomType;
import com.github.therapi.runtimejavadoc.BaseJavadoc;
import com.github.therapi.runtimejavadoc.RuntimeJavadoc;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

public class AbstractModelExporter extends AbstractSourceGenerator {
    public static class TypeInfo {
        private Class<?> clazz;
        private Type genericSubType;
        private String genericName;

        public TypeInfo(Class<?> clazz) {
            this.clazz = clazz;
        }

        public TypeInfo(Class<?> clazz, Type genericSubType) {
            this.clazz = clazz;
            this.genericSubType = genericSubType;
        }

        public TypeInfo(String genericName) {
            this.genericName = genericName;
        }

        public Class<?> getClazz() {
            return clazz;
        }

        public String getGenericName() {
            return genericName;
        }

        public Type getGenericSubType() {
            return genericSubType;
        }
    }

    protected final TypescriptGenerator typescriptGenerator;
    protected final Class<?> clazz;
    protected final String className;
    protected final String baseDir;


    public AbstractModelExporter(TypescriptGenerator typescriptGenerator, Class<?> clazz, String name, String baseDir) {
        this.typescriptGenerator = typescriptGenerator;
        this.clazz = clazz;
        this.className = name;
        this.baseDir = baseDir;
    }

    public Class<?> getClazz() {
        return clazz;
    }

    public String getClassName() {
        return className;
    }

    public String getBaseDir() {
        return baseDir;
    }

    protected boolean isRequired(AccessibleObject element, Class<?> type) {
        if (type.isPrimitive()) {
            return true;
        }
        return element.isAnnotationPresent(javax.validation.constraints.NotNull.class)
                || element.isAnnotationPresent(jakarta.validation.constraints.NotNull.class)
                || element.isAnnotationPresent(jakarta.annotation.Nonnull.class);
    }

    protected String constant(String value) {
        return "'" + value + "'";
        // not worth yet
        // return typescriptGenerator.registerOrGetConstantRef(value);
    }

    public Collection<String> tsImports(ExportTypescript exportTypescriptConfig, Class<?> type, Type genericType, boolean addI18n, boolean importAny) {
        LinkedHashSet<String> ret = new LinkedHashSet<>();
        if (exportTypescriptConfig != null && exportTypescriptConfig.addImports().length > 0) {
            ret.addAll(List.of(exportTypescriptConfig.addImports()));
        }

        if (ResponseEntity.class.isAssignableFrom(type)) {
            TypeInfo typeInfo = getType(genericType, 0);
            ret.addAll(tsImports(exportTypescriptConfig, typeInfo.getClazz(), typeInfo.getGenericSubType(), addI18n, importAny));
            return ret;
        }
        if (LocalDate.class.isAssignableFrom(type)) {
            ret.addAll(Arrays.asList(
                    "import { LocalDateConverter } from '@jmodel2ts/json2typescript/common-json-converter'",
                    "import { LocalDate } from '@js-joda/core'"));
            return ret;

        }
        if (LocalTime.class.isAssignableFrom(type)) {
            ret.addAll(Arrays.asList(
                    "import { LocalTimeConverter } from '@jmodel2ts/json2typescript/common-json-converter'",
                    "import { LocalTime } from '@js-joda/core'"));
            return ret;

        }
        if (LocalDateTime.class.isAssignableFrom(type)) {
            ret.addAll(Arrays.asList(
                    "import { LocalDateTimeConverter } from '@jmodel2ts/json2typescript/common-json-converter'",
                    "import { LocalDateTime } from '@js-joda/core'"));
            return ret;

        }
        if (ZonedDateTime.class.isAssignableFrom(type)) {
            ret.addAll(Arrays.asList(
                    "import { ZonedDateTimeConverter } from '@jmodel2ts/json2typescript/common-json-converter'",
                    "import { ZonedDateTime } from '@js-joda/core'"));
            return ret;

        }
        if (type == Comparable.class) {
            ret.add("import { Any } from '@jmodel2ts/json2typescript/any'");
            return ret;

        }
        if (type == Map.class) {
            TypeInfo typeInfoKey = getType(genericType, 0);
            TypeInfo typeInfoValue = getType(genericType, 1);

            if (exportTypescriptConfig == null || exportTypescriptConfig.addImports().length < 1) {
                ret.add("import { MapConverter } from '@jmodel2ts/json2typescript/common-json-converter'");
            }
            ret.addAll(tsImports(exportTypescriptConfig, typeInfoKey.getClazz(), typeInfoKey.getGenericSubType(), false, false));
            ret.addAll(tsImports(exportTypescriptConfig, typeInfoValue.getClazz(), typeInfoValue.getGenericSubType(), false, false));
            return ret;

        }
        if (type == Duration.class) {
            ret.add("import { Duration } from '@js-joda/core'");
            return ret;
        }
        if (type == LocalDateTime.class) {
            ret.add("import { LocalDateTime } from '@js-joda/core'");
            return ret;

        }
        if (type == LocalDate.class) {
            ret.add("import { LocalDate } from '@js-joda/core'");
            return ret;
        }
        if (type == LocalTime.class) {
            ret.add("import { LocalTime } from '@js-joda/core'");
            return ret;
        }
        if (type == ZonedDateTime.class) {
            ret.add("import { ZonedDateTime } from '@js-joda/core'");
            return ret;
        }
        if (Collection.class.isAssignableFrom(type)) {
            TypeInfo typeInfo = getType(genericType, 0);
            if (typeInfo.getClazz() != null) {
                ret.addAll(tsImports(exportTypescriptConfig, typeInfo.getClazz(), typeInfo.getGenericSubType(), false, false));
            }
            return ret;
        }
        CustomType customType = typescriptGenerator.findCustomTypeFor(type);
        if (customType != null) {
            Collection<String> customImports = customType.getImports(typescriptGenerator, this, Objects.requireNonNullElse(genericType, type), addI18n, exportTypescriptConfig);
            if (customImports != null) {
                ret.addAll(customImports);
                return ret;
            }
        }
        if (type.isArray()) {
            ret.addAll(tsImports(exportTypescriptConfig, type.getComponentType(), null, false, false));
            return ret;
        }
        if (isAnnotationPresent(type, ExportTypescript.class)) {
            if (type.isEnum()) {
                ret.addAll(Arrays.asList(
                        String.format("import { %s } from '%s'",
                                typescriptGenerator.createName(type),
                                typescriptGenerator.createFilename(type, baseDir)),
                        String.format("import { %sConverter } from '%s'",
                                typescriptGenerator.createName(type),
                                typescriptGenerator.createFilename(type, baseDir))
                ));
            } else if (genericType != null) {
                TypeInfo typeInfo = getType(genericType, 0);
                if (typeInfo.getClazz() != null) {
                    ret.addAll(tsImports(exportTypescriptConfig, typeInfo.getClazz(), typeInfo.getGenericSubType(), false, false));
                    ret.add(String.format("import { %s%s } from '%s'",
                            typescriptGenerator.createName(type),
                            addI18n ? (", " + typescriptGenerator.createName(type) + "I18n") : "",
                            typescriptGenerator.createFilename(type, baseDir)));
                }
            } else {
                ret.add(
                        String.format("import { %s%s } from '%s'",
                                typescriptGenerator.createName(type),
                                addI18n ? (", " + typescriptGenerator.createName(type) + "I18n") : "",
                                typescriptGenerator.createFilename(type, baseDir))
                );
            }
            return ret;
        }
        if (importAny && type == Object.class) {
            ret.add("import { Any } from '@jmodel2ts/json2typescript/any'");
            return ret;
        }
        return ret;
    }

    protected String tsTypeToDecoratorType(String tsType) {
        if (tsType == null) {
            return null;
        }

        boolean isArray = tsType.endsWith("[]");
        if (isArray) {
            tsType = tsType.substring(0, tsType.length() - 2);
        }

        return switch (tsType) {
            case "string" -> "String";
            case "number" -> "Number";
            case "boolean" -> "Boolean";
            case "void" -> "Void";
            default -> tsType;
        };

    }

    public String tsType(String name, TypeInfo typeInfo, Type genericType, boolean withGenericsInfo, Map<String, String> genericsReplacement) {
        if (typeInfo.getClazz() != null) {
            Class<?> type = typeInfo.getClazz();

            CustomType customType = typescriptGenerator.findCustomTypeFor(type);

            if (ResponseEntity.class.isAssignableFrom(type)) {
                type = getType(genericType, 0).getClazz();
            }

            if (type == void.class
                    || type == Void.class) {
                return "void";
            }
            if (type == InputStream.class
                    || type == InputStreamResource.class
                    || Resource.class.isAssignableFrom(type)) {
                return "url";
            }
            if (type == String.class
                    || type == Class.class
                    || type == UUID.class) {
                return "string";
            }
            if (type == Comparable.class || type == Object.class) {
                return "any";
            }
            if (type == boolean.class
                    || type == Boolean.class) {
                return "boolean";
            }
            if (type == Number.class
                    || type == byte.class
                    || type == int.class || type == Integer.class
                    || type == long.class || type == Long.class
                    || type == double.class || type == Double.class
                    || type == float.class || type == Float.class
                    || type == short.class || type == Short.class
                    || BigDecimal.class.isAssignableFrom(type)) {
                return "number";
            }
            if (type == Enum.class) {
                // TODO: if we know how to do better
                return "string";
            }
            if (type == Duration.class) {
                return "Duration";
            }
            if (type == LocalDate.class) {
                return "LocalDate";
            }
            if (type == LocalTime.class) {
                return "LocalTime";
            }
            if (type == LocalDateTime.class) {
                return "LocalDateTime";
            }
            if (type == ZonedDateTime.class) {
                return "ZonedDateTime";
            }
            if (type == MultipartFile.class) {
                return "File";
            }
            if (type == byte[].class) {
                return "Blob";
            }
            if (Map.class.isAssignableFrom(type)) {
                return String.format("Map<%s, %s>",
                        tsType("key of " + name, getType(genericType, 0), null, true,
                                buildGenericsReplacement(getGenericType(genericType, 0))),
                        tsType("value of " + name, getType(genericType, 1), null, true,
                                buildGenericsReplacement(getGenericType(genericType, 1)))
                );
            }
            if (Collection.class.isAssignableFrom(type)) {
                return tsType(name, getType(genericType, 0), null, true, genericsReplacement) + "[]";
            }
            if (customType != null) {
                String customTypeTs = customType.tsType(typescriptGenerator, this, name, genericType, genericsReplacement);
                if (customTypeTs != null) {
                    return customTypeTs;
                }
            }
            if (type.isArray()) {
                return tsType(name, new TypeInfo(type.getComponentType()), null, true, null) + "[]";
            }
            if (isAnnotationPresent(type, ExportTypescript.class)) {
                if (withGenericsInfo) {
                    StringBuilder typeStringBuilder = new StringBuilder();
                    for (int i = 0; i < type.getTypeParameters().length; i++) {
                        if (typeStringBuilder.length() == 0) {
                            typeStringBuilder.append("<");
                        } else {
                            typeStringBuilder.append(", ");
                        }

                        String genericName = type.getTypeParameters()[i].getName();
                        if (genericsReplacement != null) {
                            genericName = genericsReplacement.getOrDefault(genericName, genericName);
                        }

                        String tsType = tsType(name, new TypeInfo(genericName), null, true, genericsReplacement);
                        typeStringBuilder.append(tsType);
                    }

                    if (typeStringBuilder.length() != 0) {
                        typeStringBuilder.append(">");

                        return typescriptGenerator.createName(type) + typeStringBuilder;
                    }
                }

                return typescriptGenerator.createName(type);

            }

            throw new IllegalArgumentException(name + ": " + type.getName());
        }
        if (typeInfo.getGenericName() != null) {
            String genericName = typeInfo.getGenericName();
            if (genericsReplacement != null && genericsReplacement.containsKey(genericName)) {
                genericName = genericsReplacement.get(typeInfo.getGenericName());
            }
            return genericName;
        }
        {
            throw new IllegalArgumentException("cannot resolve tsType for " + name);
        }
    }

    private Map<String, String> buildGenericsReplacement(Type type) {
        Map<String, String> ret = new HashMap<>();
        if (type instanceof ParameterizedType pType) {
            Class<?> rawType = (Class<?>) pType.getRawType();
            for (int i = 0; i < rawType.getTypeParameters().length; i++) {
                String rawName = rawType.getTypeParameters()[i].getName();
                Type argType = pType.getActualTypeArguments()[i];
                if (argType instanceof WildcardType) {
                    ret.put(rawName, "any");
                } else if (argType instanceof Class) {
                    ret.put(rawName, tsType(null, new TypeInfo((Class<?>) argType), argType, true, null));
                }
            }
        }
        return ret;
    }

    public String jsonType(ExportTypescript exportTypescriptConfig, TypeInfo typeInfo, Type genericType, boolean stripArray, boolean modelClassAsString) {
        if (exportTypescriptConfig != null && exportTypescriptConfig.jsonConverter().length() > 0) {
            return exportTypescriptConfig.jsonConverter();
        }

        if (typeInfo.getClazz() != null) {
            CustomType customType = typescriptGenerator.findCustomTypeFor(typeInfo.getClazz());
            if (customType != null) {
                String customTypeJs = customType.jsonType(typescriptGenerator, this, exportTypescriptConfig, Objects.requireNonNullElse(genericType, typeInfo.getClazz()), stripArray);
                if (customTypeJs != null) {
                    return customTypeJs;
                }
            }

            Class<?> type = typeInfo.getClazz();
            if (type == void.class
                    || type == Void.class) {
                return "void";
            }
            if (type == String.class
                    || type == Class.class
                    || type == UUID.class) {
                return "String";
            }
            if (type == Comparable.class || type == Object.class) {
                return "Any";
            }
            if (type == boolean.class || type == Boolean.class) {
                return "Boolean";
            }
            if (type == byte[].class) {
                return "Blob";
            }
            if (type == Number.class
                    || type == byte.class
                    || type == int.class || type == Integer.class
                    || type == long.class || type == Long.class
                    || type == double.class || type == Double.class
                    || type == float.class || type == Float.class
                    || type == short.class || type == Short.class
                    || BigDecimal.class.isAssignableFrom(type)) {
                return "Number";
            }
            if (Duration.class == type) {
                return "DurationConverter";
            }
            if (LocalDate.class == type) {
                return "LocalDateConverter";
            }
            if (LocalTime.class == type) {
                return "LocalTimeConverter";
            }
            if (LocalDateTime.class == type) {
                return "LocalDateTimeConverter";
            }
            if (ZonedDateTime.class == type) {
                return "ZonedDateTimeConverter";
            }
            if (Map.class.isAssignableFrom(type)) {
                return "MapConverter";
            }
            if (Collection.class.isAssignableFrom(type)) {
                if (stripArray) {
                    return jsonType(exportTypescriptConfig, getType(genericType, 0), null, false, modelClassAsString);
                } else {
                    TypeInfo genericTypeInfo = getType(genericType, 0);
                    if (genericTypeInfo.getClazz() != null && !Object.class.equals(genericTypeInfo.getClazz())) {
                        return "[" + jsonType(exportTypescriptConfig, genericTypeInfo, null, false, modelClassAsString) + "]";
                    } else {
                        return "[]";
                    }

                }
            }
            if (type.isArray()) {
                return "[" + jsonType(exportTypescriptConfig, new TypeInfo(type.componentType()), null, false, modelClassAsString) + "]";
            }
            if (isAnnotationPresent(type, ExportTypescript.class)) {
                if (type.isEnum()) {
                    return typescriptGenerator.createName(type) + "Converter";
                }
                String typeName = typescriptGenerator.createName(type);
                if (modelClassAsString) {
                    return "'" + typeName + "'";
                }
                return typeName;
            } else if (ResponseEntity.class.isAssignableFrom(type)) {
                return jsonType(exportTypescriptConfig, getType(genericType, 0), null, false, modelClassAsString);
            }
            throw new IllegalArgumentException(type.getName() + " ... is @ExportTypescript annotation missing?");
        }
        if (typeInfo.getGenericName() != null) {
            //TODO
            return typeInfo.getGenericName();
        }

        throw new IllegalArgumentException("cannot resolve jsonType");
    }

    protected boolean isAnnotationPresent(Class<?> type, Class<? extends Annotation> annotation) {
        if (type == null) {
            return false;
        }

        Class<?> base = type;
        do {
            if (base.isAnnotationPresent(annotation)) {
                return true;
            }

            base = base.getSuperclass();
        } while (base != null);

        return false;
    }

    private Type getGenericType(Type genericType, int index) {
        if (genericType instanceof ParameterizedType parameterizedType) {
            Type[] types = parameterizedType.getActualTypeArguments();
            if (types != null && types.length > index) {
                return types[index];
            }
        }

        return null;
    }

    @SuppressWarnings("rawtypes")
    public TypeInfo getType(Type genericType, int index) {
        if (genericType instanceof ParameterizedType parameterizedType) {
            Type[] types = parameterizedType.getActualTypeArguments();
            if (types != null && types.length > index) {
                if (types[index] instanceof TypeVariable typeVariable) {
                    return new TypeInfo(typeVariable.getName());
                } else if (types[index] instanceof Class typeClass) {
                    return new TypeInfo(typeClass);
                } else if (types[index] instanceof WildcardType wildcardType) {
                    return new TypeInfo((Class) wildcardType.getUpperBounds()[0]);
                } else if (types[index] instanceof ParameterizedType subType) {

                    if (Arrays.stream(subType.getActualTypeArguments())
                            .allMatch(type -> type instanceof ParameterizedType || type instanceof Class)) {
                        return new TypeInfo((Class) subType.getRawType(), subType);
                    }

                    return new TypeInfo((Class) subType.getRawType());
                } else {
                    throw new IllegalArgumentException("Cannot resolve Type of " + genericType + " #" + index);
                }
            }
        }

        return new TypeInfo(Object.class);
    }

    protected String methodToFieldName(String methodName) {
        String fieldName = null;
        if (methodName.startsWith("get")) {
            fieldName = methodName.substring(3);
        } else if (methodName.startsWith("is")) {
            fieldName = methodName.substring(2);
        }
        if (fieldName == null) {
            throw new IllegalArgumentException("dont know how to convert '" + methodName + "' to a field name.");
        }

        fieldName = Character.toLowerCase(fieldName.charAt(0)) + fieldName.substring(1);
        return fieldName;
    }

    protected List<String> collectDoc(Method method) {
        BaseJavadoc methodJavadoc = RuntimeJavadoc.getJavadoc(method);
        return collectDoc(methodJavadoc);
    }

    protected List<String> collectDoc(Field field) {
        BaseJavadoc methodJavadoc = RuntimeJavadoc.getJavadoc(field);
        return collectDoc(methodJavadoc);
    }

    @NotNull
    private static List<String> collectDoc(BaseJavadoc methodJavadoc) {
        if (methodJavadoc == null
                || methodJavadoc.getComment() == null) {
            return Collections.emptyList();
        }

        List<String> ret = Arrays.stream(methodJavadoc.getComment().toString().split("\\n"))
                .map(comment -> {
                    if (comment.startsWith(" ")) {
                        return comment.substring(1);
                    }
                    return comment;
                })
                .dropWhile(StringUtils::isBlank)
                .toList();

        return ret;
    }
}
