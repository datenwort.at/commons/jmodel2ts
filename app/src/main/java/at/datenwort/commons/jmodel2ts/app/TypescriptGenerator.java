/*
 * Copyright (c) 2022 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package at.datenwort.commons.jmodel2ts.app;

import at.datenwort.commons.jmodel2ts.annotations.ExportTypescript;
import at.datenwort.commons.jmodel2ts.api.CustomExporter;
import at.datenwort.commons.jmodel2ts.api.CustomType;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.AccessibleObject;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class TypescriptGenerator {
    private final static Logger log = Logger.getLogger(TypescriptGenerator.class.getName());

    private final List<CustomType> customTypes = new ArrayList<>();
    private final List<CustomExporter> customExporters = new ArrayList<>();
    private Function<AccessibleObject, List<String>> additionalDocBuilder = null;

    private boolean generateRuntimeInfo = true;
    private boolean mockableServices = true;
    private boolean jsonPropertyModelClassAsString = false;
    private boolean exportClassCatalog = false;
    private Predicate<Class<?>> exportFilter;

    private LocalizationType localizationType = LocalizationType.ANGULAR_LOCALIZE;
    private ClassNameConverter classNameConverter;

    private final Reflections reflections;
    private Set<Class<?>> dtos;
    private final PropertyNamingStrategies.KebabCaseStrategy kebabCaseStrategy = new PropertyNamingStrategies.KebabCaseStrategy();
    private Path outputBase;

    public TypescriptGenerator(String... packages) {
        ConfigurationBuilder builder = new ConfigurationBuilder();

        FilterBuilder inputsFilter = new FilterBuilder();
        builder.filterInputsBy(inputsFilter);
        for (String pack : packages) {
            builder.forPackage(pack);
            inputsFilter.includePackage(pack);
        }

        reflections = new Reflections(builder);
    }

    public Function<AccessibleObject, List<String>> getAdditionalDocBuilder() {
        return additionalDocBuilder;
    }

    public void setAdditionalDocBuilder(Function<AccessibleObject, List<String>> additionalDocBuilder) {
        this.additionalDocBuilder = additionalDocBuilder;
    }

    public void addCustomType(CustomType customType) {
        customTypes.add(customType);
    }

    public CustomType findCustomTypeFor(Class<?> type) {
        CustomType ret = customTypes.stream()
                .filter(customType -> customType.isAssignableFrom(type))
                .findFirst()
                .orElse(null);
        return ret;
    }

    public CustomExporter findExporterFor(Class<?> type) {
        CustomExporter ret = customExporters.stream()
                .filter(customExporter -> customExporter.isAssignableFrom(type))
                .findFirst()
                .orElse(null);
        return ret;
    }

    public void addCustomExporter(CustomExporter customExporter) {
        customExporters.add(customExporter);
    }

    public Predicate<Class<?>> getExportFilter() {
        return exportFilter;
    }

    public void setExportFilter(Predicate<Class<?>> exportFilter) {
        this.exportFilter = exportFilter;
    }

    public boolean isGenerateRuntimeInfo() {
        return generateRuntimeInfo;
    }

    public void setGenerateRuntimeInfo(boolean generateRuntimeInfo) {
        this.generateRuntimeInfo = generateRuntimeInfo;
    }

    public boolean isMockableServices() {
        return mockableServices;
    }

    public void setMockableServices(boolean mockableServices) {
        this.mockableServices = mockableServices;
    }

    public boolean isJsonPropertyModelClassAsString() {
        return jsonPropertyModelClassAsString;
    }

    public void setJsonPropertyModelClassAsString(boolean jsonPropertyModelClassAsString) {
        this.jsonPropertyModelClassAsString = jsonPropertyModelClassAsString;
    }

    public boolean isExportClassCatalog() {
        return exportClassCatalog;
    }

    public void setExportClassCatalog(boolean exportClassCatalog) {
        this.exportClassCatalog = exportClassCatalog;
    }

    public LocalizationType getLocalizationType() {
        return localizationType;
    }

    public void setLocalizationType(LocalizationType localizationType) {
        this.localizationType = localizationType;
    }

    public ClassNameConverter getClassNameConverter() {
        return classNameConverter;
    }

    public void setClassNameConverter(ClassNameConverter classNameConverter) {
        this.classNameConverter = classNameConverter;
    }

    public void process() {
        dtos = reflections.getTypesAnnotatedWith(ExportTypescript.class).stream()
                .filter(exportable -> !exportable.isEnum()
                        && !exportable.isAnnotationPresent(RestController.class)
                        && exportable.getAnnotation(ExportTypescript.class) != null
                        && !exportable.getAnnotation(ExportTypescript.class).ignore()
                        && (exportable.getSuperclass() == null || !exportable.getSuperclass().isEnum()))
                .filter(this::shouldExport)
                .collect(Collectors.toSet());

        Map<String, String> exportedClassCatalog = new LinkedHashMap<>();

        reflections.getTypesAnnotatedWith(ExportTypescript.class).stream()
                .filter(exportable -> exportable.getAnnotation(ExportTypescript.class) != null
                        && !exportable.getAnnotation(ExportTypescript.class).ignore())
                .filter(this::shouldExport)
                .forEach(clazz -> {
                    try {
                        exportClass(clazz, exportedClassCatalog);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                });

        if (!constants.isEmpty()) {
            try {
                exportConstants(constants);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        if (isExportClassCatalog()) {
            try {
                exportClassCatalog(exportedClassCatalog);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private boolean shouldExport(Class<?> exportable) {
        if (exportFilter == null) {
            return true;
        }
        return exportFilter.test(exportable);
    }

    private void exportClass(Class<?> clazz, Map<String, String> exportedClassCatalog) throws IOException {
        CustomExporter customExporter = findExporterFor(clazz);
        if (customExporter != null) {
            exportCustom(clazz, customExporter);
            return;
        }

        if (clazz.isEnum()) {
            exportEnum(clazz);
            return;
        }

        if (clazz.isAnnotationPresent(RestController.class)) {
            exportService(clazz, dtos);
            return;
        }

        if (clazz.isInterface()) {
            exportInterface(clazz);
            return;
        }

        if (!clazz.getSuperclass().isEnum()) {
            exportDto(clazz, exportedClassCatalog);
        }
    }

    private void exportCustom(Class<?> clazz, CustomExporter customExporter) throws IOException {
        String simpleName = createName(clazz);
        String baseDir = customExporter.getBaseDir();

        final AbstractModelExporter tools = new AbstractModelExporter(this, clazz, simpleName, baseDir);

        Path output = outputBase.resolve(baseDir).resolve(kebabCaseStrategy.translate(simpleName) + ".ts");
        log.log(Level.INFO, "Export: {0}", output.toAbsolutePath().toString());
        Files.createDirectories(output.getParent());
        try (PrintWriter writer = new PrintWriter(Files.newBufferedWriter(output, StandardCharsets.UTF_8))) {
            customExporter.export(this, tools, clazz, simpleName, baseDir, writer);
        }
    }

    private void exportConstants(Map<String, String> constants) throws IOException {

        Path output = outputBase.resolve("constants.ts");
        log.log(Level.INFO, "Export: {0}", output.toAbsolutePath().toString());
        Files.createDirectories(output.getParent());
        try (PrintWriter writer = new PrintWriter(Files.newBufferedWriter(output, StandardCharsets.UTF_8))) {
            writer.println("export class Constants {");
            for (Map.Entry<String, String> constantsEntry : new TreeMap<>(constants).entrySet()) {
                writer.println(String.format("  public static readonly %s = '%s'",
                        constantsEntry.getValue(),
                        constantsEntry.getKey()));
            }
            writer.println("}");
        }
    }

    private void exportClassCatalog(Map<String, String> exportedClasses) throws IOException {

        Path output = outputBase.resolve("lib/jmodel2ts-class-catalog.ts");
        log.log(Level.INFO, "Export: {0}", output.toAbsolutePath().toString());
        Files.createDirectories(output.getParent());
        try (PrintWriter writer = new PrintWriter(Files.newBufferedWriter(output, StandardCharsets.UTF_8))) {
            for (String exportedClassImport : exportedClasses.values()) {
                writer.println(exportedClassImport);
            }

            writer.println("export const JMODEL2TS_CLASS_CATALOG = [");
            for (String exportedClass : exportedClasses.keySet()) {
                writer.println(String.format("  %s,", exportedClass));
            }
            writer.println("]");
        }
    }

    private void exportEnum(Class<?> clazz) throws IOException {
        String simpleName = createName(clazz);

        Path output = outputBase.resolve("enums").resolve(kebabCaseStrategy.translate(simpleName) + ".enum.ts");
        log.log(Level.INFO, "Export: {0}", output.toAbsolutePath().toString());
        Files.createDirectories(output.getParent());
        try (PrintWriter writer = new PrintWriter(Files.newBufferedWriter(output, StandardCharsets.UTF_8))) {
            new EnumExporter(this, clazz, simpleName, "enums")
                    .export(writer);
        }
    }

    private void exportService(Class<?> clazz, Set<Class<?>> dtos) throws IOException {
        String simpleName = createName(clazz);

        Path output = outputBase.resolve("services").resolve(kebabCaseStrategy.translate(simpleName) + ".service.ts");
        log.log(Level.INFO, "Export: {0}", output.toAbsolutePath().toString());
        Files.createDirectories(output.getParent());
        try (PrintWriter writer = new PrintWriter(Files.newBufferedWriter(output, StandardCharsets.UTF_8))) {
            new ServiceExporter(this, clazz, simpleName, "services", dtos)
                    .export(writer);
        }
    }

    private void exportDto(Class<?> clazz, Map<String, String> exportedClassCatalog) throws IOException {
        String simpleName = createName(clazz);

        Path baseDir = outputBase.resolve("model");
        Path output = baseDir.resolve(kebabCaseStrategy.translate(simpleName) + ".ts");
        log.log(Level.INFO, "Export: {0}", output.toAbsolutePath().toString());
        Files.createDirectories(output.getParent());
        try (PrintWriter writer = new PrintWriter(Files.newBufferedWriter(output, StandardCharsets.UTF_8))) {
            exportedClassCatalog.put(simpleName,
                    String.format("import { %s } from '%s'",
                            createName(clazz),
                            createFilename(clazz, baseDir.toString())));

            new ModelExporter(this, clazz, simpleName, "model")
                    .export(writer);
        }
    }

    private void exportInterface(Class<?> clazz) throws IOException {
        String simpleName = createName(clazz);

        Path output = outputBase.resolve("model").resolve(kebabCaseStrategy.translate(simpleName) + ".ts");
        log.log(Level.INFO, "Export: {0}", output.toAbsolutePath().toString());
        Files.createDirectories(output.getParent());
        try (PrintWriter writer = new PrintWriter(Files.newBufferedWriter(output, StandardCharsets.UTF_8))) {
            new InterfaceExporter(this, clazz, simpleName, "model")
                    .export(writer);
        }
    }

    @NotNull
    public String createName(Class<?> clazz) {
        String simpleName = getClassNameConverter().getNgClassName(clazz);

        return simpleName;
    }

    @NotNull
    public String createFilename(Class<?> clazz, String baseDir) {
        String base = kebabCaseStrategy.translate(createName(clazz));
        if (clazz.isAnnotationPresent(RestController.class)) {
            base = ("services".equals(baseDir) ? "./" : "../services/") + base + ".service";
        } else if (clazz.isEnum()) {
            base = ("enums".equals(baseDir) ? "./" : "../enums/") + base + ".enum";
        } else {
            base = ("model".equals(baseDir) ? "./" : "../model/") + base;
        }
        return base;
    }

    public void setOutput(Path outputBase) {
        if (!Files.exists(outputBase) || !Files.isDirectory(outputBase)) {
            throw new IllegalArgumentException("base " + outputBase + " invalid.");
        }
        this.outputBase = outputBase;
    }

    public Path getOutputBase() {
        return outputBase;
    }

    public String createSelectorName(String className) {
        return kebabCaseStrategy.translate(className);
    }

    private final Map<String, String> constants = new ConcurrentHashMap<>();
}
