/*
 * Copyright (c) 2022 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package at.datenwort.commons.jmodel2ts.app;

import at.datenwort.commons.jmodel2ts.annotations.ExportTypescript;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Comparator;

public class EnumExporter extends AbstractModelExporter {
    public EnumExporter(TypescriptGenerator typescriptGenerator, Class<?> clazz, String name, String baseDir) {
        super(typescriptGenerator, clazz, name, baseDir);
    }

    public void export(PrintWriter writer) {
        String className = typescriptGenerator.createName(clazz);

        ExportTypescript exportTypescript = clazz.getAnnotation(ExportTypescript.class);
        boolean valueFromToString = exportTypescript.enumValueFromToString();

        writer.println("import { JsonConverter } from '@jmodel2ts/json2typescript/json-convert-decorators'");
        writer.println("import { EnumConverter } from '@jmodel2ts/json2typescript/common-json-converter'");
        writer.println("import { Component, Input, Pipe, PipeTransform } from '@angular/core'");
        if (exportTypescript.generateTeplateDirectives()) {
            writer.println("import { SelectionListItemContext, SelectionListItemDirective } from '@shared/modules/ui/selection-list/selection-list-item.directive'");
            writer.println("import { SelectOptionContext, SelectOptionDirective } from '@shared/modules/forms/controls/select/select.component'");
            writer.println("import { Directive, forwardRef, TemplateRef } from '@angular/core'");
        }

        writer.println();
        writer.println("// noinspection JSUnusedGlobalSymbols");
        writer.format("export enum %s {\n", className);
        int count = 0;
        //noinspection unchecked
        Enum<?>[] enumConstants = ((Class<? extends Enum<?>>) clazz).getEnumConstants();
        Arrays.sort(enumConstants, Comparator.comparing(Enum::ordinal));
        for (Enum<?> enumConstant : enumConstants) {
            if (count > 0) {
                writer.println(",");
            }
            writer.format("  %s = '%s'", enumConstant.name(), valueFromToString ? enumConstant.toString() : enumConstant.name());
            count++;
        }
        writer.println();
        writer.println("}");

        writer.println();

        if (valueFromToString) {
            writer.format("enum _reverse_%s {\n", className);
            count = 0;
            Arrays.sort(enumConstants, Comparator.comparing(Enum::ordinal));
            for (Enum<?> enumConstant : enumConstants) {
                if (count > 0) {
                    writer.println(",");
                }
                writer.format("  '%s' = %s.%s", enumConstant.toString(), className, enumConstant.name());
                count++;
            }
            writer.println();
            writer.println("}");

            writer.println();
        }

        writer.println("@JsonConverter");
        writer.format("export class %sConverter extends EnumConverter<%s> {\n", className, className);
        writer.format("  fromStr(data: string): %s {\n", className);
        if (valueFromToString) {
            writer.format("    return _reverse_%s[data as keyof typeof _reverse_%s] as any as %s\n", className, className, className);
        } else {
            writer.format("    return %s[data as keyof typeof %s]\n", className, className);
        }
        writer.println("  }");
        writer.println();
        writer.format("  toStr(data: %s): string {\n", className);
        writer.format("    return %s[data]\n", className);
        writer.println("  }");
        writer.println("}");

        if (exportTypescript.i18n()) {
            if (typescriptGenerator.getLocalizationType() == LocalizationType.ANGULAR_LOCALIZE) {
                writer.println();
                writer.format("export class %sI18n {\n", className);
                for (Enum<?> enumConstant : enumConstants) {
                    String name = enumConstant.name();
                    writer.format("  public static readonly %s = $localize`:@@enums.%s.%s:` || $localize`:@@globals.enums.%s:%s`\n",
                            name, className, name,
                            name, name);
                }
                writer.println("}");
                writer.println();
                writer.println("@Component({");
                writer.println("  // tslint:disable-next-line:component-selector");
                writer.format("  selector: '%s-i18n',\n", typescriptGenerator.createSelectorName(className));
                writer.println("  standalone: true,");
                writer.format("  template: `{{ text(key) }}`\n");
                writer.println("})");
                writer.format("export class %sI18nComponent {\n", className);
                writer.println("  @Input()");
                writer.format("  key: %s\n", className);
                writer.println();
                writer.println(String.format("  text(key: %s): string {", className));
                writer.format("    return %sI18n[key] || $localize`:@@enums.not_defined:No translation`\n", className);
                writer.println("  }");

                writer.println("}");

                writer.println();

                writer.format("""
                                @Pipe({
                                  name: '%sI18n',
                                  standalone: true
                                })
                                export class %sI18nPipe implements PipeTransform {
                                  transform(key: %s, ...args: any[]): any {
                                    if (key == null) {
                                      return null
                                    }
                                                 
                                    return %sI18n[key] || $localize`:@@enums.not_defined:No translation`
                                  }
                                }
                                """,
                        className, className, className, className);
            }
        }

        if (exportTypescript.generateTeplateDirectives()) {
            writer.println();
            writer.println("@Directive({");
            writer.println(String.format("    selector: '[app%sSelectionListItem]',", className));
            writer.println("    standalone: true,");
            writer.println("    providers: [{");
            writer.println("        provide: SelectionListItemDirective,");
            writer.println(String.format("        useExisting: forwardRef(() => %sSelectionItemDirective)", className));
            writer.println("    }]");
            writer.println("})");
            writer.println(String.format("export class %sSelectionItemDirective extends SelectionListItemDirective<%s> {", className, className));
            writer.println(String.format("    constructor(public override template: TemplateRef<SelectionListItemContext<%s>>) {", className));
            writer.println("        super(template)");
            writer.println("    }");
            writer.println("}");
            writer.println();
            writer.println("@Directive({");
            writer.println(String.format("    selector: '[app%sSelectOption]',", className));
            writer.println("    standalone: true,");
            writer.println("    providers: [{");
            writer.println("        provide: SelectOptionDirective,");
            writer.println(String.format("        useExisting: forwardRef(() => %sSelectOptionDirective)", className));
            writer.println("    }]");
            writer.println("})");
            writer.println(String.format("export class %sSelectOptionDirective extends SelectOptionDirective<%s> {", className, className));
            writer.println(String.format("    constructor(public override template: TemplateRef<SelectOptionContext<%s>>) {", className));
            writer.println("        super(template)");
            writer.println("    }");
            writer.println("}");
        }
    }
}
