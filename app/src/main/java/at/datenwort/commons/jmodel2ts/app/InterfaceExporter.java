/*
 * Copyright (c) 2022 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package at.datenwort.commons.jmodel2ts.app;

import at.datenwort.commons.jmodel2ts.annotations.ExportTypescript;
import at.datenwort.commons.jmodel2ts.annotations.TsImport;
import at.datenwort.commons.jmodel2ts.annotations.TsMethod;

import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.TypeVariable;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

public class InterfaceExporter extends AbstractModelExporter {

    public InterfaceExporter(TypescriptGenerator typescriptGenerator, Class<?> clazz, String name, String model) {
        super(typescriptGenerator, clazz, name, model);
    }

    public void export(PrintWriter writer) {
        ExportTypescript exportTypescript = getAnnotation(clazz, ExportTypescript.class);

        writer.println("// noinspection ES6UnusedImports");

        Set<String> imports = new TreeSet<>();

        Map<String, String> fieldsTsTypes = new LinkedHashMap<>();
        Map<String, Method> fields = new LinkedHashMap<>();
        Map<String, List<String>> fieldsDocs = new LinkedHashMap<>();
        Set<String> fieldsForTypeCheck = new LinkedHashSet<>();
        List<Method> declaredMethods = getDeclaredMethods(clazz);
        for (Method method : declaredMethods) {
            if (Modifier.isStatic(method.getModifiers())) {
                continue;
            }

            if (method.getParameterCount() > 1) {
                // not supported yet
                continue;
            }

            if (!method.getName().startsWith("get")
                    && !method.getName().startsWith("is")) {
                continue;
            }

            String fieldName = methodToFieldName(method.getName());
            if (!fieldsTsTypes.containsKey(fieldName)) {
                fields.put(fieldName, method);
                fieldsTsTypes.put(fieldName,
                        tsType(
                                method.getName(),
                                new TypeInfo(method.getReturnType()),
                                method.getGenericReturnType(),
                                true,
                                null));
                fieldsDocs.put(fieldName, collectDoc(method));

                if (isRequired(method, method.getReturnType())) {
                    fieldsForTypeCheck.add(fieldName);
                }
            }

            imports.addAll(tsImports(
                    method.getAnnotation(ExportTypescript.class),
                    method.getReturnType(), method.getGenericReturnType(), false, true));
        }
        for (Class<?> extendedInterface : clazz.getInterfaces()) {
            imports.addAll(tsImports(null, extendedInterface, null, false, true));
        }

        imports.stream()
                .filter(Objects::nonNull)
                .forEach(writer::println);

        for (TsImport tsImport : exportTypescript.imports()) {
            writer.println(tsImport.value());
        }

        writer.println();
        writer.format("export interface %s", className);

        StringBuilder typeStringBuilder = new StringBuilder();
        for (TypeVariable<? extends Class<?>> typeVariable : clazz.getTypeParameters()) {
            if (typeStringBuilder.length() == 0) {
                typeStringBuilder.append("<");
            } else {
                typeStringBuilder.append(", ");
            }
            typeStringBuilder.append(typeVariable.getName());
        }

        if (typeStringBuilder.length() != 0) {
            typeStringBuilder.append(">");

            writer.print(typeStringBuilder);
        }

        writer.print(" ");

        boolean extendsAdded = false;
        for (Class<?> extendsInterface : clazz.getInterfaces()) {
            if (extendsInterface.isAnnotationPresent(ExportTypescript.class)) {
                String extendsInterfaceName = typescriptGenerator.createName(extendsInterface);

                if (!extendsAdded) {
                    extendsAdded = true;
                    writer.print(" extends ");
                } else {
                    writer.print(", ");
                }
                writer.format(" %s", extendsInterfaceName);
            }
        }
        if (extendsAdded) {
            writer.print(" ");
        }

        writer.println("{");

        int count = 0;
        for (Map.Entry<String, String> fieldsTsTypesEntry : fieldsTsTypes.entrySet()) {
            if (count > 0) {
                writer.println();
            }

            Method method = fields.get(fieldsTsTypesEntry.getKey());
            List<String> docs = fieldsDocs.get(fieldsTsTypesEntry.getKey());
            if (docs != null && !docs.isEmpty()) {
                writer.println("  /**");
                for (String doc : docs) {
                    writer.print("   * ");
                    writer.println(doc);
                }
                writer.println("   */");
            }

            writer.format("  %s%s: %s\n",
                    fieldsTsTypesEntry.getKey(),
                    isRequired(method, method.getReturnType()) ? "" : "?",
                    fieldsTsTypesEntry.getValue());

            count++;
        }

        for (TsMethod method : exportTypescript.methods()) {
            writer.println();
            writer.println(method.value());
        }

        writer.println("}");

        writer.println();

        writer.format("export function instanceOf%s(value: any): value is %s {\n", className, className);
        writer.format("  return value != null && Object.getPrototypeOf(value).constructor._implements_%s\n",
                clazz.getName().replace(".", "_"));
        writer.println("}");

        writer.println();
    }
}
