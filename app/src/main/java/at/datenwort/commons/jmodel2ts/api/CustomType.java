/*
 * Copyright (c) 2022 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package at.datenwort.commons.jmodel2ts.api;

import at.datenwort.commons.jmodel2ts.annotations.ExportTypescript;
import at.datenwort.commons.jmodel2ts.app.AbstractModelExporter;
import at.datenwort.commons.jmodel2ts.app.ServiceExporter;
import at.datenwort.commons.jmodel2ts.app.TypescriptGenerator;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface CustomType {
    boolean isAssignableFrom(Class<?> type);

    Class<?> getType();

    default Collection<String> getImports(TypescriptGenerator typescriptGenerator, AbstractModelExporter exporter, Type type, boolean addI18n, ExportTypescript exportTypescriptConfig) {
        // use default impl
        return null;
    }

    default boolean writeService(TypescriptGenerator typescriptGenerator, ServiceExporter serviceExporter, Class<?> type, ServiceExporter.RequestBodyHelper requestBodyHelper, PrintWriter writer, PrintWriter writerInterface, Method method, List<ServiceExporter.ReqParameter> reqParameters, Map<String, String> genericsReplacement, String tsReturnType, String httpContext, String httpParams, String urlParams, RequestMethod requestMethod, String servicePath) {
        // use default impl
        return false;
    }

    default String asParameter(TypescriptGenerator generator, ServiceExporter serviceExporter, Class<?> type, String name, boolean required, TypescriptGenerator typescriptGenerator) {
        // use default impl
        return null;
    }

    default Class<?> findBestMatchingDto(ServiceExporter serviceExporter, Class<?> type, TypescriptGenerator typescriptGenerator) {
        // use default impl
        return null;
    }

    default String tsType(TypescriptGenerator typescriptGenerator, AbstractModelExporter exporter, String name, Type type, Map<String, String> genericsReplacement) {
        // use default impl
        return null;
    }

    default String jsonType(TypescriptGenerator typescriptGenerator, AbstractModelExporter exporter, ExportTypescript exportTypescriptConfig, Type type, boolean stripArray) {
        // use default impl
        return null;
    }

    default List<ServiceExporter.ReqParameter> getRequestParameters(TypescriptGenerator typescriptGenerator, ServiceExporter serviceExporter, Type type) {
        // use default impl
        return null;
    }
}
