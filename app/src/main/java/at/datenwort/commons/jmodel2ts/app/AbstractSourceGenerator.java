/*
 * Copyright (c) 2022 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package at.datenwort.commons.jmodel2ts.app;

import at.datenwort.commons.jmodel2ts.annotations.ExportTypescript;
import org.apache.bcel.Repository;
import org.apache.bcel.classfile.JavaClass;
import org.springframework.core.annotation.MergedAnnotation;
import org.springframework.core.annotation.MergedAnnotations;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class AbstractSourceGenerator {
    public <A extends Annotation> A getAnnotation(Class<?> clazz, Class<A> annotation) {
        Class<?> base = clazz;

        do {
            A ann = base.getAnnotation(annotation);
            if (ann != null) {
                return ann;
            }

            base = base.getSuperclass();
        }
        while (base != null);

        return null;
    }

    public <A extends Annotation> A findAnnotation(Annotation[] parameterAnnotations, Class<A> annotation) {
        if (parameterAnnotations == null) {
            return null;
        }

        MergedAnnotation<A> requestParam = MergedAnnotations.from(parameterAnnotations).get(annotation);
        if (requestParam.isPresent()) {
            return requestParam.synthesize();
        }

        return null;
    }

    protected List<Field> getDeclaredFields(Class<?> clazz) {
        Map<String, Integer> order = new HashMap<>();
        try {
            JavaClass bcelClazz = Repository.lookupClass(clazz);
            for (org.apache.bcel.classfile.Field field : bcelClazz.getFields()) {
                order.put(field.getName(), order.size());
            }
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        List<Field> fields = Arrays.asList(clazz.getDeclaredFields());
        fields.sort((f1, f2) -> {
            int i1 = Objects.requireNonNullElse(order.get(f1.getName()), Integer.MAX_VALUE);
            int i2 = Objects.requireNonNullElse(order.get(f2.getName()), Integer.MAX_VALUE);
            return Integer.compare(i1, i2);
        });

        return fields;
    }

    protected List<Method> getDeclaredMethods(Class<?> clazz) {
        Map<MethodKey, Integer> order = new HashMap<>();
        try {
            JavaClass bcelClazz = Repository.lookupClass(clazz);
            for (org.apache.bcel.classfile.Method method : bcelClazz.getMethods()) {
                order.putIfAbsent(MethodKey.from(method), order.size());
            }
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        return Stream.of(clazz.getDeclaredMethods())
                .filter(method -> {
                    ExportTypescript exportTypescript = method.getAnnotation(ExportTypescript.class);
                    if (exportTypescript != null) {
                        return !exportTypescript.ignore();
                    }
                    return true;
                })
                .sorted((f1, f2) -> {
                    int i1 = Objects.requireNonNullElse(order.get(MethodKey.from(f1)), Integer.MAX_VALUE);
                    int i2 = Objects.requireNonNullElse(order.get(MethodKey.from(f2)), Integer.MAX_VALUE);
                    return Integer.compare(i1, i2);
                })
                .collect(Collectors.toList());
    }

    private record MethodKey(String name, String parameters) {

        static MethodKey from(org.apache.bcel.classfile.Method method) {
                return new MethodKey(
                        method.getName(),
                        Arrays.stream(method.getArgumentTypes()).map(org.apache.bcel.generic.Type::toString).collect(Collectors.joining(","))
                );
            }

            static MethodKey from(Method method) {
                return new MethodKey(
                        method.getName(),
                        Arrays.stream(method.getParameterTypes()).map(Class::getName).map(MethodKey::fixArrays).collect(Collectors.joining(","))
                );
            }

        //findByKey
        //at.datenwort.excited.persistence.api.ObjectId[],at.datenwort.zoones.noModel.po.NmCommDepartment,boolean,at.datenwort.zoones.noModel.po.NmZone,boolean,at.datenwort.zoones.aos.ng.enums.VirtualCommissionFilter,java.lang.String,at.datenwort.zoones.aos.ng.lib.Enrichment
        //[Lat.datenwort.excited.persistence.api.ObjectId;,at.datenwort.zoones.noModel.po.NmCommDepartment,boolean,at.datenwort.zoones.noModel.po.NmZone,boolean,at.datenwort.zoones.aos.ng.enums.VirtualCommissionFilter,java.lang.String,at.datenwort.zoones.aos.ng.lib.Enrichment


        private static String fixArrays(String ori) {
            if(ori.startsWith("[L") && ori.endsWith(";")) {
                return ori.substring(2, ori.length()-1) + "[]";
            }
                return ori;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                MethodKey methodKey = (MethodKey) o;
                return Objects.equals(name, methodKey.name) && Objects.equals(parameters, methodKey.parameters);
            }

    }
}
